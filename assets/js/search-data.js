var store = [{
        "title": "Verschlüsselung im Chaos (Chaos-based encryption)",
        "excerpt":"Verschlüsselung im Chaos “A copy of this text I received in the late 1980s in Harare (Zimbabwe) by the mathematician Sönke Rehder; back then as a pupil I implemented (“coded”) the herein described algorithm in BASIC on an Atari 600XL. Sönke Rehder has worked out the algorithm and has written...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesSecurity","Security","Cryptography","Encrypt","Decrypt","Chaos","ChaosBasedCryptography","ChaosBasedEncryption","PoincaréFunction","Algorithm","BareMetal","Serverless","Java","Maven","SönkeRehder"],
        "url": "https://www.metacodes.pro/blog/chaos-based_encryption/"
      },{
        "title": "Autumn leaves, Terminate and Stay Resident!",
        "excerpt":"In the year 1991 it was not very common in old Germany that we pupils in eleventhgrade had computer science classes at school. As unusual as the computer science lessons were for us students, those lessons were even more unusual for our teachers.Few of my mates and me already had...","categories": ["blog"],
        "tags": ["Metacodes","Funcodes","AssemblyCode","80x86","X86","TSR","TerminateAndStayResident","RetroComputing","MSDOS","DOS"],
        "url": "https://www.metacodes.pro/blog/autumn_leaves_terminate_and_stay_resident/"
      },{
        "title": "Listings im Kilo-Pack: Die Dateien von heute",
        "excerpt":"I was very proud when in October 1992 the computer magazine »DOS International«published a 1024 bytes short Turbo Pascalprogram I wrote and for which I won 1024 DM,one German mark per byte! It was fun to tinker with the computers at those times and discover new possibilities everyday, while endless...","categories": ["blog"],
        "tags": ["Metacodes","Funcodes","Pascal","TurboPascal","DosInternational","ListingsImKiloPack","1024Wettbewerb","RetroComputing","MSDOS","DOS"],
        "url": "https://www.metacodes.pro/blog/die_dateien_von_heute/"
      },{
        "title": "Computer Archeology: Exploring the Anatomy of an MS-DOS Virus",
        "excerpt":"Thanks to an encounter by chance with two casual middle aged gentlemen at the Communication Convention Center (C3) »Mitte« early one morning and convincing them that their story deserved documentation, I had them unveil a highly idiosyncratic hacker’s time capsule, transporting me back to the world of 1990s malware. Analyzing...","categories": ["blog"],
        "tags": ["Metacodes","Funcodes","AssemblyCode","80x86","X86","TSR","TerminateAndStayResident","MSDOS","MS-DOS","Virus","Malware","RetroComputing","ComputerArcheology","1990s"],
        "url": "https://www.metacodes.pro/blog/computer_archeology_exploring_the_anatomy_of_an_ms_dos_virus/"
      },{
        "title": "jatelite P2P Middleware",
        "excerpt":"Die jatelite P2P Middleware ist ein Framework für Applikationen. Sie verbindet Computer unterschiedlichster Geräteklassen miteinander und ermöglicht so die Entwicklung komplexer Kommunikationsnetzwerke. Die Middleware bietet die Möglichkeit, Software mit Peer-To-Peer, Client/Server oder beliebiger andere Topologie zu entwickeln. Sie ist komponentenbasiert und Rahmengerüst für Applikationen, ähnlich einem Application Server. Sie ist...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/jatelite_p2p_middleware/"
      },{
        "title": "Content Exchange Network",
        "excerpt":"Das CEN (Content Exchange Network) ist eine Peer-To-Peer (P2P) Applikation, mit der Inhalte in einem Netzwerk aus Anbietern und Konsumenten den Teilnehmern verfügbar gemacht werden. Da die Middleware die Möglichkeit bietet, bestehende Systeme mit P2P Fähigkeiten auszurüsten, haben wir ein Content Management System und Java SWING Applikationen in das CEN...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/content_exchange_network/"
      },{
        "title": "Intranet Communicator",
        "excerpt":"Der Intranet Communicator ist eine modulare Messenger-Software für Desktops auf Peer-To-Peer Basis. Hier steht die Kommunikation von Personen im Vordergrund. Der Intranet Communicator bildet die Basis für Peer-To-Peer (P2P) Applikatione: Dritt-Entwickler hatten Zugriff auf die Quellcodes der Software für einen schnellen Einstieg in die Programmierung mit der jatelite Middleware. Der...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/intranet_communicator/"
      },{
        "title": "jSphere - Qualitätsmanagement für Flughafen Ground-Handler",
        "excerpt":"jSphere ist eine Software zur Qualitätssicherung für einen Flughafen Ground-Handler, der eine ausbaufähige verteilte Applikation zur Qualitätssicherung in Auftrag gegeben hat. Der Ground-Handler ist für logistische Aufgaben ‘am Boden’ im Auftrag von Fluggesellschaften verantwortlich. Dabei gilt es, die Qualitätsstandards einzuhalten. Unterschiedliche Abteilungen in verschiedenen Ländern sollen mit Hilfe der Software...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/jsphere_qualit%C3%A4tsmanagement_fuer_flughafen_ground_handler/"
      },{
        "title": "Intranetlösung zur Projektplanung",
        "excerpt":"Bei der Intranetlösung handelt es sich um eine Applikation im Bereich Projektplanung eines auf Industrial Design spezialisierten Unternehmens. Unterschiedlichste Benutzergruppen kalkulieren Projekte und verwalten ihre Urlaube mit dieser Software. Der Client, ein sogenannter Rich Client, wurde als Macromedia Flash Applikation implementiert und kommuniziert über Java Web-Services mit dem Server. Thema...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/intranetloesung_zur_projektplanung/"
      },{
        "title": "Content Management System Migration",
        "excerpt":"Die Organisation hat ihre Web-Site auf das Open Source CMS Typo3 migriert. Dabei wurde die Technologie auf einem bei einem Internet Provider gehosteten Server installiert. Designs und Templates einer Grafik-Agentur wurden implementiert und das System zur Verwendung von Redakteuren vorbereitet. Thema Typo3 Content Management System (CMS) Migration Branche Öffentlicher Dienst...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/content_management_system_migration/"
      },{
        "title": "Online Produkt-Konfigurator",
        "excerpt":"Der Anwender wird Schritt für Schritt durch die Konfiguration seines Produktes geführt. Das Produkt wird aus mehreren Einzelteilen zusammengesetzt und vom Anbieter später kundenspezifisch gefertigt. Am Ende des Konfigurationsvorganges kann der Anwender den Bestellvorgang auslösen. Bei der Internetlösung handelt es sich um eine JSP Webanwendung. Interessant ist hier die Kombinatorik...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/online_produkt_konfigurator/"
      },{
        "title": "WMmobil",
        "excerpt":"Der WM’06 Planer ist eine Anwendung für Handys, über die ein Anwender minutenschnell über den aktuellen Stand laufender WM Spiele informiert wird – Ereignisse wie etwa gelbe Karten, Auswechselungen und Tore aller aktuellen und der schon stattgefundenen Spiele stehen dem Anwender so jederzeit und überall zur Verfügung. Ein Simulationsmodus (was-wäre-wenn)...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/wmmobil/"
      },{
        "title": "Mobile Sportliga Framework",
        "excerpt":"Um die Funktionalität der WMmobil Applikation (siehe unten) für jede Art von Sportliga verfügbar zu haben, wurde WMmobil so erweitert, dass jede Art von Sportliga als Handy-Applikation erstellt werden kann. Mit diesem Baukastensystem ist es möglich, Versionen z.B. für die Italienische Fußballliga oder eine Handballliga zu erstellen. Wir haben hier...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/mobile_sportliga_framework/"
      },{
        "title": "News2Mobile (Feedreader)",
        "excerpt":"Eine große Anzahl von Webseiten stellt die Inhalte als so genannte Nachrichten-Feeds zur Verfügung. Dabei handelt es sich um eine maschinell lesbare Version der Inhalte (Nachrichten mit Überschriften und Text) dieser Webseiten. Der Feedreader stellt die Inhalte von vielen solcher Webseiten in kompakter Form dem Anwender auf dem Handy zur...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/news2mobile_feedreader/"
      },{
        "title": "Mobile Feedreader Framework",
        "excerpt":"Bei diesem Projekt handelt es sich um eine Erweiterung des Feedreader Projektes (siehe unten). Hier haben wir wieder ein Baukastensystem, mit dem kundenspezifische Lösungen zum Konsumieren von Website Inhalten für Handys erstellen werden. Im Gegensatz zum Feedreader Projekt gibt es zusätzlich eine Vielzahl Stellschrauben, um das Produkt den Kundenwünschen (Verlagshäuser)...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/mobile_feedreader_framework/"
      },{
        "title": "IT-Consulting: Java-EE Projekte im Bereich Automotive",
        "excerpt":"Bei diesen Projekten handelt es sich um Anwendungen, die im Produktionsprozess und im Bereich der Verwaltung eines großen Automobilherstellers eingesetzt werden. Das Backend diese Anwendungen basiert auf EJB 1.4 mit einem Struts basierten Web-Frontend, bei der Entwicklung wurde u.a ein MDA basierter Entwicklungsprozess verwendet. Thema Java-EE Anwendungen zur Unterstützung unternehmensinterner...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/it_consulting_java_ee_projekte_im_bereich_automotive/"
      },{
        "title": "Campaign Mangement: Produktentwicklung im Cloud Computing Umfeld (Big Data)",
        "excerpt":"Diese Tätigkeit besteht darin, ein bestehendes Team aufzubauen, so dass der Grundstein für eine Java EE basierte Produktentwicklung gelegt ist. Hier habe ich einen Entwicklungsprozess (von der Anforderung bis zur Abnahme) zusammen mit einem Vorgehensmodell (“Scrum”) eingeführt. Darüber hinaus habe ich eine SW-Architektur erarbeitet sowie verschiedene Konventionen, die eine Software-Entwicklung...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/campaign_mangement_produktentwicklung_im_cloud_computing_umfeld_big_data/"
      },{
        "title": "Banking-Portal Architektur und Entwicklung sowie Security-Lead",
        "excerpt":"In der Rolle des Portal-Architekten galt es, das Portal einer Bank architektonisch fortzuentwickeln und von einem Monolithen hin zu einer Modularen Architektur zu begleiten. Als Security-Lead bezüglich Sicherheit und OWASP war dafür Sorge zu tragen, dass entsprechende Security-Audits aufgrund vorbeireitenden Maßnahmen erfolgreich durchgeführt werden und ein entsprechendes Reporting zu etablieren....","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/banking_portal_architektur_und_entwicklung_sowie_security_lead/"
      },{
        "title": "Erarbeiten und Einführung einer Microservice-Architektur",
        "excerpt":"Aufgrund der Erfahrungen mit monolithischen Architekturen besteht der Wunsch des Auftraggebers, ein neues Architekturkonzept zu erarbeiten, das eine größtmögliche Skalierbarkeit in verschiedene Richtungen ermöglicht (Skalierung technisch / horizontal, Skalierung der Entwicklerteams, Skalierung der Umsetzung insgesamt); dieses Architekturkonzept wird in einem ersten Projekt zur Produktivreife geführt und gemeinsam mit Operations Live...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/erarbeiten_und_einf%C3%BChrung_einer_microservice_architektur/"
      },{
        "title": "Coding Serbia 2014 in Novi Sad - Big data processing the lean way",
        "excerpt":"In October ‘14, I was happy to get a slot as a speaker at the Coding Serbia Conference 2014 , presenting my paper on Big data processing the lean way - a case study (Slides). All the sessions are published on the Coding Serbia channel on YouTube Thank all of...","categories": ["blog"],
        "tags": ["Metacodes","CodingSerbia","CodingSerbia2014","Conference","Speaker","ConferenceSpeaker","2014","NoviSad","Serbia","CaseStudy","BigData","CloudComputing","AWS","Java"],
        "url": "https://www.metacodes.pro/blog/coding_serbia_2014/"
      },{
        "title": "METACODES.PRO: Open Source Software auf Maven Central und Bitbucket",
        "excerpt":"Im Rahmen meiner langjährigen Tatigkeit als SW-Entwickler stelle ich fortlaufend nützliche und wiederkehrende Problemstellungen in Form von Java Bibliotheken oder Tools als Open Source zur Verfügung. Diese Artefakte werden auf Bitbucket gehostet und können über Maven Central global in Projekte eingebunden werden. Als Anlaufstelle für die Java Maven-Artefakte und Tools...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/metacodes_pro_open_source_software_auf_maven_central_und_bitbucket/"
      },{
        "title": "Hello REFCODES.ORG!",
        "excerpt":"Hello World! The REFCODES.ORG blog just opened; it’s purpose is to accompany my org.refcodes Maven artifacts published on Maven Central (“org.refcodes”). The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers...","categories": ["blog"],
        "tags": ["Metacodes","OpenSource","Java","Maven"],
        "url": "https://www.metacodes.pro/blog/hello_world/"
      },{
        "title": "refcodes-cli: Parse your args[]",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact defines some helpful toolkit to parse your command line arguments...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesConsole","Args","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-cli/"
      },{
        "title": "refcodes-command: Do the undo",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? Basically a Command can be seen as a method turned inside out:...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesCommand","Java","Maven","BareMetal","Serverless","Command","Pattern","CommandPattern"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-command/"
      },{
        "title": "Having fun with the command pattern",
        "excerpt":"“… (with) the command pattern … an object is used to represent and encapsulate all the information needed to call a method at a later time. This … includes the … method parameters …”1 A Command can be seen as a method (using Java’s terminology) and its context (arguments and...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesCommand","Java","Maven","BareMetal","Serverless","Command","Pattern","CommandPattern","Interface"],
        "url": "https://www.metacodes.pro/blog/having_fun_with_the_command_pattern/"
      },{
        "title": "refcodes-forwardsecrecy: High encryption throughput",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifacts provides you means to produce high throughput of symmetric encryption...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesForwardsecrecy","ForwardSecrecy","Cryptography","BareMetal","Serverless","CloudComputing","Java","Maven"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-forwardsecrecy/"
      },{
        "title": "refcodes-logger: Fancy runtime-logs and highly scalable cloud logging",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides flexible logging of any data to any data sink...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesLogger","Console","Logger","NoSQL","CloudComputing","ANSI","BareMetal","Serverless","Java","Maven","SLF4J","Log4J"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-logger/"
      },{
        "title": "Logging like the nerds log",
        "excerpt":"Having set up your spring-boot microservice and want to switch to the refcodes-logger tool-box because you want to have some fancy log output and want to log into a NoSQL database later on? Here is a quick start on how to enable the refcodes-logger-alt-console logger which produces fancy output. After...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesLogger","Console","Logger","NoSQL","CloudComputing","ANSI","Java","Maven","BareMetal","Serverless","SpringBoot","SLF4J","Log4J"],
        "url": "https://www.metacodes.pro/blog/logging_like_the_nerds_log/"
      },{
        "title": "refcodes-remoting: Face-to-face lightweight remote method calls",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact is a lightweight RPC (remote procedure call) toolkit (you may...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesRemoting","RPC","IPC","Lightweight","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-remoting/"
      },{
        "title": "refcodes-security: Chaos-based encryption as JCE (and without)",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides the basic interfaces (and supporting classes) to implement vanilla...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesSecurity","Security","Cryptography","Encrypt","Decrypt","Chaos","ChaosBasedCryptography","ChaosBasedEncryption","PoincaréFunction","JavaCryptographicExtension","Algorithm","JCE","BareMetal","Serverless","Java","Maven","SönkeRehder"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-security/"
      },{
        "title": "refcodes-eventbus: Observer + Publish/Subscribe = Message broker",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifacts implements a lightweight infrastructure for event based decoupled communication. This...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesEventbus","RefcodesObserver","BareMetal","Serverless","ObserverPattern","PublishSubscribe","PublishSubscribePattern","MessageBroker","Observer","Observable","Listener","EventListener","Event","Java","Maven"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-eventbus/"
      },{
        "title": "Publish ... subscribe ... observe ... event-bus?",
        "excerpt":"Lately some colleagues and me were reasoning on how to do client-side (Browser, JavaScript) communication between web-page components … decoupled … in general I recommend taking a look at an event-bus (aka page-bus), with Java why not take a look at the refcodes-eventbus? … the following “equation” came to mind...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesEventbus","RefcodesObserver","ObserverPattern","PublishSubscribePattern","MessageBroker","Observer","Observable","Listener","EventListener","Event","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/blog/publish_subscribe_observe_event-bus/"
      },{
        "title": "refcodes-jobbus: Asynchronous job execution",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact makes use of the Command pattern and provides a frame...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesJobbus","RefcodesCommand","Asynchronous","Java","Maven","BareMetal","Serverless","Command","Pattern","CommandPattern"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-jobbus/"
      },{
        "title": "Get your jobs executed asynchronously - anywhere!",
        "excerpt":"Asynchronous execution has revived some kind of hype nowadays. Be it as of *design constraints [1] and performance considerations [2] regarding the underlying runtime (such as JavaScript) or upcoming runtime independent requirements processing big data.* Latter forces developers to process huge amounts of data on different distributed systems and therewith...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesJobbus","RefcodesCommand","Asynchronous","BigData","Java","Maven","BareMetal","Serverless","Command","Pattern","CommandPattern","REST","RESTful"],
        "url": "https://www.metacodes.pro/blog/get_your_jobs_executed_asynchronously/"
      },{
        "title": "Java 8: Obliged to do the Optional ... or is it optional?",
        "excerpt":"There is some controversy going on around the Java community regarding the Optional type introduced by Java 8. It has been around before1 though now it’s official. Something I hear during daily business lately here and there: “… never return null, return an Optional object …” +++ “… you won’t...","categories": ["blog"],
        "tags": ["Refcodes","Java8","Java","Optional","Controversy","REST","RESTful"],
        "url": "https://www.metacodes.pro/blog/obliged_to_do_the_optional_or_is_it_optional/"
      },{
        "title": "Lambda up your jobs asynchronously!",
        "excerpt":"The refcodes-jobbus artifact has already been introduced by the blog Get your jobs executed asynchronously - anywhere! as well as by the article refcodes-jobbus: Asynchronous job execution. As of the version 1.0.0 coming next, Java 8’s lambda support has been added to the execute method of the JobBus. Try it...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesJobbus","RefcodesCommand","Lambda","Asynchronous","BigData","Java","Maven","BareMetal","Serverless","Command","Pattern","CommandPattern","Lambda","Closure"],
        "url": "https://www.metacodes.pro/blog/lambda_up_your_jobs_asynchronously/"
      },{
        "title": "Of software development methodologies, teams and individuals",
        "excerpt":"Whilst pushing new methodologies into a software development team, one should understand the difference between methodologies of team relevance and of individual taste… At least in my small software developer’s world, I observe some controversy on how a team is to do software development the right way and which methodologiesa...","categories": ["blog"],
        "tags": ["Metacodes","Methodology","Agile","Scrum","TDD","TestDrivenDevelopment","Kanban","CodeKata","CodingDojo","GTD","GettingThingsDone","Controversy","Interface"],
        "url": "https://www.metacodes.pro/blog/of_software_development_methodologies_teams_and_individuals/"
      },{
        "title": "REFCODES.ORG change list version 1.0.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:48:52. Change list &lt;refcodes-licensing&gt; (version 1.0.0) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 1.0.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.0) [ADDED] DateUtility.java (see Javadoc at DateUtility.java) [DELETED] DateUtility.java [MODIFIED] pom.xml [MODIFIED] ConcurrentDateFormat.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.0/"
      },{
        "title": "Off topic summertime workshops",
        "excerpt":"Summer break is over and I wasn’t too lazy. This summer I started some workshops not only on creative computing … such as getting an electrified tethered flight plane into the air, refurbishing my MERKUR Vision arcade machine and doing some thinking on BoulderDash being a cellular automaton (but this...","categories": ["blog"],
        "tags": ["Metacodes","TetheredFlight","ArcadeMachine","MerkurVision","CellularAutomaton","RefcodesCheckerboard"],
        "url": "https://www.metacodes.pro/blog/off_topic_summertime_workshops/"
      },{
        "title": "JavaFX checkerboard for fun and cellular automaton experiments",
        "excerpt":"In the mid 80’s I was playing around with Atari BASIC, GFA BASIC and Turbo Pascal. At that time I was also eagerly reading articles published in the magazine Spektrum der Wissenschaft from my father’s bookshelf. I was especially interested in articles belonging to the columns Mathematische Unterhaltung (Mathematical Games)...","categories": ["blog"],
        "tags": ["Refcodes","Funcodes","Checkerboard","CellularAutomaton","SwarmIntelligence","Java","JavaFX","Maven","RefcodesCheckerboard"],
        "url": "https://www.metacodes.pro/blog/java_fx_checkerboard_for_fun_and_cellular_automaton_experiments/"
      },{
        "title": "REFCODES.ORG change list version 1.0.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:48:56. Change list &lt;refcodes-licensing&gt; (version 1.0.1) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 1.0.1) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.1) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.0.1) [ADDED] VerifyRuntimeException.java (see Javadoc at VerifyRuntimeException.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.1/"
      },{
        "title": "Base! How low can you go? Base64, Base32, Base16, ...",
        "excerpt":"I don’t like utility classes. Somehow they seem to fill up with functionality soon forgotten and never to be found again. While refactoring lots of utility classes I came across some Base64 related functionality in one of them which delegated its functionality to some third party API … Some words...","categories": ["blog"],
        "tags": ["Refcodes","Base64","Codec","Encode","Decode","BuilderPattern","UtilityClass","UtilityBuilderPattern","Java","Maven","BareMetal","Serverless","RefcodesCodec"],
        "url": "https://www.metacodes.pro/blog/base_how_low_can_you_go_base64_base32_base16/"
      },{
        "title": "REFCODES.ORG change list version 1.0.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:48:59. Change list &lt;refcodes-licensing&gt; (version 1.0.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 1.0.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.2) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.0.2) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.0.2) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.2/"
      },{
        "title": "Good bye utility classes, here come builders (1/2)",
        "excerpt":"This is part 1 of a two parts series on using a weak variant of the builder pattern with Java instead of using utility classes. This part discusses creating ASCII tables using a builder. You may want to jump to Part 2 being on ASCII art or go on with...","categories": ["blog"],
        "tags": ["Refcodes","AsciiArt","ASCII","Table","BuilderPattern","UtilityClass","UtilityBuilderPattern","Interface","Java","Maven","RefcodesTextual"],
        "url": "https://www.metacodes.pro/blog/good_bye_utility_classes_here_come_builders_(1)/"
      },{
        "title": "Good bye utility classes, here come builders (2/2)",
        "excerpt":"This is part 2 of a two parts series on using a weak variant of the builder pattern with Java instead of using utility classes. This part discusses creating ASCII art using a builder. You may start off with Part 1 being on ASCII tables or go on with the...","categories": ["blog"],
        "tags": ["Refcodes","AsciiArt","ASCII","BuilderPattern","UtilityClass","UtilityBuilderPattern","Java","Maven","RefcodesTextual"],
        "url": "https://www.metacodes.pro/blog/good_bye_utility_classes_here_come_builders_(2)/"
      },{
        "title": "REFCODES.ORG change list version 1.0.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:02. Change list &lt;refcodes-licensing&gt; (version 1.0.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.0.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.3) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.0.3) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.0.3) [ADDED] CharSetAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.3/"
      },{
        "title": "Yet another REST cheat sheet",
        "excerpt":"I believe that good interfaces are even more important than good code. When refactoring a bad interface, code behind (providers) that interface and code before (consumers) that interface has to be refactored (not to forget the interface itself). Weighting even more heavily the more systems use the bad interface. When...","categories": ["blog"],
        "tags": ["Metacodes","REST","RESTful","API","Microservice","Design","Interface","CheatSheet"],
        "url": "https://www.metacodes.pro/blog/yet_another_rest_cheat_sheet/"
      },{
        "title": "No more Mr. Nice Guy, no more code reviews!!!",
        "excerpt":"Call it software architecture, call it software design or call it programming motherf••ker: Interfaces are everywhere, be it your method signature on an API level, be it your RESTful service on a microservice’s level or be it your command line arguments of your shell script. So why is everybody talking...","categories": ["blog"],
        "tags": ["Metacodes","REST","RESTful","API","Interface","Design","Controversy"],
        "url": "https://www.metacodes.pro/blog/no_more_mr_nice_gue_no_more_code_review/"
      },{
        "title": "The utility-builder pattern",
        "excerpt":"Combining a Utility classs functionality with a weak variant of the builder pattern, add some spicy properties to it, and here comes the utility-builder pattern. Utility classes are the container for lots of useful but lost code. Hard to find and hard to extend. The utility-builder (aka tool-builder) pattern provides...","categories": ["blog"],
        "tags": ["Metacodes","CheatSheet","BuilderPattern","UtilityClass","UtilityBuilderPattern"],
        "url": "https://www.metacodes.pro/blog/the_utility-builder_pattern/"
      },{
        "title": "REFCODES.ORG change list version 1.0.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:06. Change list &lt;refcodes-licensing&gt; (version 1.0.4) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.0.4) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.4) [MODIFIED] pom.xml [MODIFIED] log4j.xmlChange list &lt;refcodes-exception&gt; (version 1.0.4) [MODIFIED] pom.xml [MODIFIED] log4j.xmlChange list &lt;refcodes-mixin&gt; (version...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.4/"
      },{
        "title": "Better programs - Citations one should have heard of (1/2)",
        "excerpt":"Ever been confronted with name-dropping while being on a programming related discussion? This two-part series on Citations one should have heard of brings some light into the depth of name-dropping and why the background behind those terms helps you getting a better programmer. I arranged the bits and pieces in...","categories": ["blog"],
        "tags": ["Metacodes","CheatSheet","ArithmeticIf","KISS","ConwaysLaw","Encapsulation","ActorModel","Cohesion","CallByValueResult","ACID","RealProgrammers","LawOfDemeter","CAP","DRY"],
        "url": "https://www.metacodes.pro/blog/better_programs-citations_one_should_have_heard_of_(1)/"
      },{
        "title": "Better programs - Citations one should have heard of (2/2)",
        "excerpt":"Ever been confronted with name-dropping while being on a programming related discussion? This two-part series on Citations one should have heard of brings some light into the depth of name-dropping and why the background behind those terms helps you getting a better programmer. I arranged the bits and pieces in...","categories": ["blog"],
        "tags": ["Metacodes","CheatSheet","StranglerApplication","PortsAndAdapters","PortsAndAdaptersArchitecture","SOLID","Viscosity","MicroservicePrerequisites","SISO"],
        "url": "https://www.metacodes.pro/blog/better_programs-citations_one_should_have_heard_of_(2)/"
      },{
        "title": "REFCODES.ORG change list version 1.0.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:09. Change list &lt;refcodes-licensing&gt; (version 1.0.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.0.5) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.0.5) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.0.5) [MODIFIED] pom.xml [MODIFIED] ExceptionUtility.java (see Javadoc at ExceptionUtility.java)Change list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.0.5/"
      },{
        "title": "Development-Driven Incident-Management",
        "excerpt":"Das Development-Team war zu befähigen, Incidents im Betrieb einer auf Kafka basierenden Messaging-Lösung zu anylysieren und zu beheben (im Sinne von DevOps). Die Messaging-Lösung ist für den Massenversand von Kunden bezogenen Nachrichten (etwa Buchungsbestätigungen, Rechnungen oder Notifications) zuständig. Es galt also das Entwickler-Team hin zu DevOps zu transformieren. Dabei wurde...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/development_driven_incident_management/"
      },{
        "title": "Code-Review Automotive",
        "excerpt":"Bei einem lang andauernden Projekt (&gt;5 Jahre) war ein Code-Review durchzuführen, um proaktiv auf Herausforderungen bezüglich Performance und Wartbarkeit eingehen zu können. Thema Code-Reiew bezüglich Performance und Wartbarkeit eines langlaufenden Projekts auf JEE-Basis mit Eclipse Rich Client Platform Branche Automotive Rolle Gutachter Dauer 9.2016 – 10.2016 Tätigkeiten Entwickler-Interviews, Code-Review, Code-Metriken...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/code_review_automotive/"
      },{
        "title": "REFCODES.ORG change list version 1.1.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:13. Change list &lt;refcodes-licensing&gt; (version 1.1.0) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.0) [ADDED] DateUtility.java (see Javadoc at DateUtility.java) [ADDED] ConcurrentDateFormat.java (see Javadoc at ConcurrentDateFormat.java) [ADDED] TimeUnit.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.1.0/"
      },{
        "title": "REFCODES.ORG change list version 1.1.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:18. Change list &lt;refcodes-licensing&gt; (version 1.1.1) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.1) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.1) [ADDED] DateFormat.java (see Javadoc at DateFormat.java) [ADDED] DateFormats.java (see Javadoc at DateFormats.java) [ADDED] TimeAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.1.1/"
      },{
        "title": "refcodes-rest: RESTful services using lambdas",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? With this artifact you easily create your serverless RESTful services and REST...","categories": ["refcodes"],
        "tags": ["Refcodes","RefcodesRest","RefcodesNet","REST","RESTful","Lightweight","Lambda","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-rest/"
      },{
        "title": "Bare-Metal REST with just a few lines of code",
        "excerpt":"Find out about an alternative approach on how you can implement a serverless, tiny and slim RESTful service or a tiny and slim REST client with a little help from Java’s lambda expressions. Moreover, we skip using Heavy-Weight frameworks doing magic in the background. Instead we will follow the classic...","categories": ["blog"],
        "tags": ["Refcodes","REST","RESTful","Java","Lambda","IoT","InternetOfThings","RefcodesRest","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/bare_metal_rest_with_just_a_view_lines_of_code/"
      },{
        "title": "REFCODES.ORG change list version 1.1.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:21. Change list &lt;refcodes-licensing&gt; (version 1.1.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.2) [MODIFIED] pom.xml [MODIFIED] DateFormat.java (see Javadoc at DateFormat.java) [MODIFIED] DateUtility.java (see Javadoc at DateUtility.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_version_1.1.2/"
      },{
        "title": "Erstellen von Microservices mit Spring-Boot sowie Team-Coaching",
        "excerpt":"Wahlhelfer in den Wahllokalen waren mit einer portablen Lösung bestehend aus Hardware („Wahlkoffer“) und Software auszustatten, um die Durchführung von Wahlen einer Landeshauptstadt organisatorisch zu begleiten (z.B. zu Erfassung, Aggregation und Übermittlung von Statistiken, Anwesenheiten oder Auszählungsstatus). Neben der eigentlichen Umsetzung der Microservices galt es, Berufseinsteiger im Projekt bezüglich Spring...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/erstellen_von_microservices_mit_spring_boot_sowie_team_coaching/"
      },{
        "title": "REFCODES.ORG change list version 1.1.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:25. Change list &lt;refcodes-licensing&gt; (version 1.1.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.3) [MODIFIED] pom.xml [MODIFIED] DateFormat.java (see Javadoc at DateFormat.java)Change list &lt;refcodes-exception&gt; (version 1.1.3) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.3/"
      },{
        "title": "REFCODES.ORG change list version 1.1.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-04-26 at 21:49:28. Change list &lt;refcodes-licensing&gt; (version 1.1.4) [MODIFIED] pom.xml [MODIFIED] REFCODESChange list &lt;refcodes-parent&gt; (version 1.1.4) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.4) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.1.4) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.1.4) [ADDED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.4/"
      },{
        "title": "REFCODES.ORG change list version 1.1.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-05-05 at 22:41:07. Change list &lt;refcodes-licensing&gt; (version 1.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.1.5) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.5/"
      },{
        "title": "REFCODES.ORG change list version 1.1.6",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-05-15 at 21:32:31. Change list &lt;refcodes-licensing&gt; (version 1.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.1.6) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.6/"
      },{
        "title": "REFCODES.ORG change list version 1.1.7",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-07-25 at 20:32:32. Change list &lt;refcodes-licensing&gt; (version 1.1.7) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.7) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.7) [MODIFIED] pom.xml [MODIFIED] DateFormat.java (see Javadoc at DateFormat.java) [MODIFIED] DateFormats.java (see Javadoc at DateFormats.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.7/"
      },{
        "title": "REFCODES.ORG change list version 1.1.8",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-11-01 at 20:57:27. Change list &lt;refcodes-licensing&gt; (version 1.1.8) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.8) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.8) [MODIFIED] pom.xml [MODIFIED] DateFormats.java (see Javadoc at DateFormats.java) [MODIFIED] TimeAccessor.java (see Javadoc at TimeAccessor.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.8/"
      },{
        "title": "REFCODES.ORG change list version 1.1.9",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-11-23 at 19:56:15. Change list &lt;refcodes-licensing&gt; (version 1.1.9) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.1.9) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.1.9) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.1.9) [MODIFIED] pom.xml [MODIFIED] AbstractException.java (see Javadoc at AbstractException.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.1.9/"
      },{
        "title": "JVM-Con 2017 in Cologne",
        "excerpt":"It has been quite some time ago since I was at the Coding Serbia Conference 2014 talking about Big Data. This year I have been at the JVM-Con 2017 in Cologne as a speaker, the topic being “About sense and nonsense on extensive framework use” by the example of Spring...","categories": ["blog"],
        "tags": ["Refcodes","JvmCon","JvmCon2017","2017","Conference","Speaker","ConferenceSpeaker","Cologne","Framework","Library","REST","RESTful","Java","SpringBoot","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/jvm_con_2017/"
      },{
        "title": "Dead simple Java application configuration",
        "excerpt":"For my projects I usually do not need a full blown technology stack at hands, so I pick just those libraries which I actually need for them. Furthermore, I do not like to infiltrate my code with annotations, heavily binding it to some specific framework. Moreover, I like my projects...","categories": ["blog"],
        "tags": ["Refcodes","Properties","Observable","Configuration","Parsing","YAML","TOML","XML","JSON","RefcodesConfiguration","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/dead_simple_java_application_configuration/"
      },{
        "title": "The canonical model, an ace upon your sleeve",
        "excerpt":"The canonical model pattern is an ace up your sleeve in order to open your libraries for functionality otherwise to be implemented in a tiresome, error prone and redundant way. As you settle upon a canonical model within your library, your library’s will be able to interact with any existing...","categories": ["blog"],
        "tags": ["Refcodes","CanonicalModel","CanonicalModelPattern","CanonicalMap","Properties","HttpBodyMap","Marshaling","Unmarshaling","RefcodesStructure","RefcodesConfiguration","RefcodesNet","RefcodesRest","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/the_canonical_model_an_ace_upon_your_sleeve/"
      },{
        "title": "REFCODES.ORG change list version 1.2.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2017-12-23 at 17:05:31. Change list &lt;refcodes-licensing&gt; (version 1.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.0) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.0/"
      },{
        "title": "refcodes-properties: Managing your application's configuration",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides a canonical model for processing properties from various different...","categories": ["refcodes"],
        "tags": ["Refcodes","Properties","Observable","Configuration","Parsing","YAML","TOML","XML","JSON","RefcodesConfiguration","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-properties/"
      },{
        "title": "Geodaten-Portal Testmanagement und Lasttests",
        "excerpt":"Ein Geodaten-Portal zur Planung und Verwaltung von Anliegen des Baureferats einer Landeshauptstadt war im Rahmen von Lasttests auf Schwachstellen und Eignung hin zu untersuchen bei Benutzung im Rahmen der Mengengerüste. Dabei waren Lastprobleme systematisch zu erkennen um die Ursachen zu identifizieren und bei deren Behebung zu unterstützen. Um dies zu...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/geodaten_portal_testmanagement_und_lasttests/"
      },{
        "title": "REFCODES.ORG change list version 1.2.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-01-09 at 22:18:01. Change list &lt;refcodes-licensing&gt; (version 1.2.1) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.1) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.1) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.2.1) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.1) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.1/"
      },{
        "title": "REFCODES.ORG change list version 1.2.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-02-14 at 11:24:48. Change list &lt;refcodes-licensing&gt; (version 1.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt; (version 1.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.2) [ADDED] InstanceIdAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.2/"
      },{
        "title": "Are frameworks in software development without alternative?",
        "excerpt":"Last year I have been at the JVM-Con 2017 in Cologne as a speaker, the topic being “About sense and nonsense on extensive framework use” by the example of Spring Boot. For anyone who is interested in a more detailled insight regarding alternatives for extensive framework use: Now the related...","categories": ["blog"],
        "tags": ["Refcodes","InformatikAktuell","2017","2018","JvmCon","JvmCon2017","Conference","Speaker","ConferenceSpeaker","Cologne","Framework","Library","REST","RESTful","Java","SpringBoot","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/sind_frameworks_alternativlos/"
      },{
        "title": "REFCODES.ORG change list version 1.2.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-03-25 at 16:18:07. Change list &lt;refcodes-licensing&gt; (version 1.2.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.3) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.3) [MODIFIED] pom.xml [MODIFIED] ObjectAccessor.java (see Javadoc at ObjectAccessor.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.3/"
      },{
        "title": "REFCODES.ORG change list version 1.2.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-04-08 at 18:01:25. Change list &lt;refcodes-licensing&gt; (version 1.2.4) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.4) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.4) [MODIFIED] pom.xml [MODIFIED] DateFormat.java (see Javadoc at DateFormat.java) [MODIFIED] DateFormats.java (see Javadoc at DateFormats.java) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.4/"
      },{
        "title": "REFCODES.ORG change list version 1.2.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-04-12 at 06:16:50. Change list &lt;refcodes-licensing&gt; (version 1.2.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.5) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.5) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.5) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 1.2.5) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.5/"
      },{
        "title": "REFCODES.ORG change list version 1.2.6",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-05-05 at 17:55:11. Change list &lt;refcodes-licensing&gt; (version 1.2.6) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.6) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.6) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.6) [DELETED] ThreadMode.java [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 1.2.6) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.6/"
      },{
        "title": "Automatically obfuscate your Java application's configuration",
        "excerpt":"Ever been worried about credentials (passwords, secrets or access keys) residing in your host’s application configuration files as plain text? Read on how to automatically obfuscate your application’s properties residing in configuration files at a system-context’s level. My blog post Dead simple Java application configuration already outlined the ease of...","categories": ["blog"],
        "tags": ["Refcodes","Properties","Obfuscate","Configuration","Runtime","RefcodesConfiguration","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/automatically_obfuscate_your_java_applications_configuration/"
      },{
        "title": "All-in-one Java configuration properties at hand",
        "excerpt":"Learn on how to use the “all-in-one” Java configuration properties giving you all the power of the various configuration features with a single line of code. Managing properties from multiple sources such as files, input streams or URLs, with a precedence applied on them, enriched with profile functionality, obfuscated and...","categories": ["blog"],
        "tags": ["Refcodes","Properties","Obfuscate","Configuration","Parsing","YAML","TOML","XML","JSON","Runtime","RefcodesConfiguration","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/all-in-one_java_configuration_properties_at_hand/"
      },{
        "title": "REFCODES.ORG change list version 1.2.7",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-05-19 at 16:53:28. Change list &lt;refcodes-licensing&gt; (version 1.2.7) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.7) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.7) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.7) [ADDED] DecryptPrefixAccessor.java (see Javadoc at DecryptPrefixAccessor.java) [ADDED] EncryptPrefixAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.7/"
      },{
        "title": "Restful JShell ... RestREPL your Java 9's JShell!",
        "excerpt":"Java 9’s JShell can not only be used as a command line tool, furthermore you can harness its functionality in your own Java applications. Learn on how to harness the JShell by building a restful service around it. The JShell is shipped with Java 9 and represents a read-eval-print loop,...","categories": ["funcodes"],
        "tags": ["Funcodes","REST","Restful","JShell","ClubFuncodes","RestRepl","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/funcodes/restful_jshell_restrepl_your_java_9_jshell/"
      },{
        "title": "REFCODES.ORG change list version 1.2.8",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-05-26 at 21:00:49. Change list &lt;refcodes-licensing&gt; (version 1.2.8) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.8) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.8) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.8) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 1.2.8) [MODIFIED] pom.xml [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.8/"
      },{
        "title": "REFCODES.ORG change list version 1.2.9",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-06-10 at 20:47:59. Change list &lt;refcodes-licensing&gt; (version 1.2.9) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.2.9) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.2.9) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.2.9) [ADDED] FilenameExtensionAccessor.java (see Javadoc at FilenameExtensionAccessor.java) [ADDED] FilenameExtensionsAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.2.9/"
      },{
        "title": "REFCODES.ORG change list version 1.3.0",
        "excerpt":"All REFCODES.ORG artifacts now have a unique and stable Automatic-Module-Name declaration in their MANIFEST.MF files. All artifacts have been prepared with a module-info.java file, being disabled (module-info.java.off) for the time being as of some non-modularized external dependencies. This change list has been auto-generated on triton by steiner with with changelist-all.sh...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","JavaModularity","JavaModules","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.0/"
      },{
        "title": "Steuerung von Heizungssystemen (IoT im Kontext technischer und fachlicher Systeme)",
        "excerpt":"IoT im Kontext technischer und fachlicher Systeme (im Sinne eines technischen Systems und in Abgrenzungen zu fachlichen Systemen) sind zwei Mikrocontroller von Typ ATmega328 für die Steuerung eines Heizungssystems mit Wärmetauscher verantwortlich, wobei diese Mikrocontroller mittels I2C miteinander und mittels MQTT über eine TTY2MQTT-Bridge auf einem RaspberryPi mit der Cloud...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/steuerung_von_heizungssystemen_iot_im_kontext_technischer_und_fachlicher_systeme/"
      },{
        "title": "Yet another BASH cheat sheet",
        "excerpt":"The tiny BASH Cheet Sheet provides you (me) with a more or less alphabetically sorted tool-box of the expressions I most commonly use and tend to forget. I will update this cheat sheet now and then depending on more expressions I can’t remember :-) Args Bash script template Echo Files...","categories": ["blog"],
        "tags": ["Metacodes","Linux","Shell","BASH","CheatSheet"],
        "url": "https://www.metacodes.pro/blog/yet_another_bash_cheat_sheet/"
      },{
        "title": "REFCODES.ORG change list version 1.3.1",
        "excerpt":"Most of the artifacts are Java 8 compliant if not otherwise required. This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-09-18 at 06:45:52. Change list &lt;refcodes-licensing&gt; (version 1.3.1) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.3.1) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.3.1) [DELETED] #Untitled-1# [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","JavaModularity","JavaModules","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.1/"
      },{
        "title": "REFCODES.ORG change list version 1.3.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-09-21 at 06:47:39. Change list &lt;refcodes-licensing&gt; (version 1.3.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.3.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.3.2) [ADDED] CookieDateFormatTest.java [MODIFIED] pom.xml [MODIFIED] DateFormat.java (see Javadoc at DateFormat.java) [MODIFIED] DateFormats.java (see Javadoc at...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","JavaModularity","JavaModules","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.2/"
      },{
        "title": "REFCODES.ORG change list version 1.3.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-09-21 at 19:24:55. Change list &lt;refcodes-licensing&gt; (version 1.3.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.3.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.3.3) [MODIFIED] pom.xml [MODIFIED] DateFormats.java (see Javadoc at DateFormats.java) [MODIFIED] CookieDateFormatTest.javaChange list &lt;refcodes-mixin&gt; (version 1.3.3) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","JavaModularity","JavaModules","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.3/"
      },{
        "title": "Authentifizierung und Autorisierung in verteilten Systemen",
        "excerpt":"Mit zunehmender digitaler Integration werden verteilte Authentifizierung und Autorisierung wie OpenID, OAuth, UMA oder JWT immer wichtiger. In diesem Innovationsprojekt habe ich untersucht, wie welcher Standard arbeitet, wann welcher Standard sinnvoll einzusetzen ist sowie welche verschiedenen Standards von welchen Technologie-Stacks unterstützt werden. Das Ergebnis ist ein Dokument, das als eBook...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/authentifizierung_und_autorisierung_in_verteilten_systemen/"
      },{
        "title": "REFCODES.ORG change list version 1.3.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-10-11 at 06:46:26. Change list &lt;refcodes-licensing&gt; (version 1.3.4) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.3.4) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.3.4) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.3.4) [ADDED] TrimAccessor.java (see Javadoc at TrimAccessor.java) [ADDED] ValidAccessor.java (see...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","JavaModularity","JavaModules","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.4/"
      },{
        "title": "REFCODES.ORG change list version 1.3.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-10-28 at 17:44:48. Change list &lt;refcodes-licensing&gt; (version 1.3.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.3.5) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.3.5) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.3.5) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 1.3.5) [MODIFIED] pom.xml [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.3.5/"
      },{
        "title": "IT-Tage 2018 - Experiences of an Enterprise Developer with IoT",
        "excerpt":"In December ‘18, I was happy to have a slot as a speaker at the IT-Tage 2018, presenting my topic “With swarm intelligence on Burglar Hunt” (“Internet of Things: Experiences of an Enterprise Developer”). From December 10, 2018 till December 13 the IT-Tage 2018 took place in Frankfurt this year....","categories": ["blog"],
        "tags": ["Refcodes","IoT","SwarmIntelligence","ITTage","ITTage2018","2018","Conference","Speaker","ConferenceSpeaker","Frankfurt","Germany","CaseStudy","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/it_tage_2018/"
      },{
        "title": "JVM-Con 2018 - With swarm intelligence on Burglar Hunt",
        "excerpt":"In December ‘18, I was happy to get a slot as a speaker at the JVM-Con 2018, presenting my topic “With swarm intelligence on Burglar Hunt” (“Internet of Things: Experiences of an Enterprise Developer”). From November 27, 2018 till November 28 the JVM-Con 2018 took place in Cologne this year....","categories": ["blog"],
        "tags": ["Refcodes","IoT","SwarmIntelligence","JvmCon","JvmCon2018","2018","Conference","Speaker","ConferenceSpeaker","Cologne","Germany","CaseStudy","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/jvm_con_2018/"
      },{
        "title": "REFCODES.ORG change list version 1.9.9",
        "excerpt":"This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-11-18 at 10:55:49. Change list &lt;refcodes-licensing&gt; (version 1.9.9) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.9.9) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.9.9) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.9.9) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 1.9.9) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.9.9/"
      },{
        "title": "REFCODES.ORG change list version 1.10.0",
        "excerpt":"Change list for REFCODES.ORG artifacts’ version 1.10.0 This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-11-25 at 07:52:58. Change list &lt;refcodes-licensing&gt; (version 1.10.0) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.10.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.10.0) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.10.0) [MODIFIED] pom.xmlChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.10.0/"
      },{
        "title": "Fun with cellular automatons, BoulderDash and the Watchdog!",
        "excerpt":"The WATCHDOG artifact is some demo application featuring a cellular automatons being built with the REFCODES.ORG artifacts. It accompanies my talk at the JVM-Con 2018, where I speak about With swarm intelligence on Burglar Hunt. The funcodes.club provides some show-cases on cellular automatons with its source codes hosted at Bitbucket,...","categories": ["funcodes"],
        "tags": ["Funcodes","JvmCon","JvmCon2018","BoulderDash","Watchdog","CellularAutomaton","SwarmmIntelligence","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/funcodes/fun_with_cellular_automatons_boulder_dash_and_the_watchdog/"
      },{
        "title": "REFCODES.ORG change list version 1.10.1",
        "excerpt":"Change list for REFCODES.ORG artifacts’ version 1.10.1 This change list has been auto-generated on triton by steiner with with changelist-all.sh on the 2018-12-24 at 19:16:03. Change list &lt;refcodes-licensing&gt; (version 1.10.1) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.10.1) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.10.1) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.10.1) [MODIFIED] pom.xmlChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.10.1/"
      },{
        "title": "Hello Papers!",
        "excerpt":"aäb cde fgh ijk lmn oöp qrsß tuü vwx yz AÄBC DEF GHI JKL MNO ÖPQ RST UÜV WXYZ !”§ $%&amp; /() =?* ‘&lt;&gt; # ; ²³~ @´ ©«» ¼× {} aäb cde fgh ijk lmn oöp qrsß tuü vwx yz AÄBC DEF GHI JKL MNO ÖPQ RST UÜV WXYZ...","categories": ["papers"],
        "tags": ["Papers","Disruptive","SiegfriedSteiner"],
        "url": "https://www.metacodes.pro/papers/hello_papers/"
      },{
        "title": "REFCODES.ORG change list version 1.10.2",
        "excerpt":"Change list for REFCODES.ORG artifacts’ version 1.10.2 This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-01-13 at 19:29:00. Change list &lt;refcodes-licensing&gt; (version 1.10.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 1.10.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.10.2) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.10.2) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.10.2/"
      },{
        "title": "Ed Post über JavaScript (1/3)",
        "excerpt":"Im folgenden Teil 1 dieses Interviews befassen wir uns mit den wilden Jahren von JavaScript, in Teil 2 sprechen wir über besondere JavaScript-Spezialitäten und in Teil 3 wagt Ed Post ein Fazit und eine Prognose zu JavaScript. Teil 1: Die wilden Jahre Ed Post1 ist wohl einer der allgemein anerkanntesten...","categories": ["blog"],
        "tags": ["Metacodes","EdPost","Interview","JavaScript","ProgrammingLanguage","GeneralPurposeLanguage","Reactive","Threading","WebAssembly","Rant"],
        "url": "https://www.metacodes.pro/blog/ed_post_ueber_javascript_(1)/"
      },{
        "title": "Ed Post über JavaScript (2/3)",
        "excerpt":"In Teil 1 dieses Interviews haben wir uns mit den wilden Jahren von JavaScript befasst, im folgenden Teil 2 sprechen wir über besondere JavaScript-Spezialitäten und in Teil 3 wagt Ed Post ein Fazit und eine Prognose zu JavaScript. Teil 2: Besondere JavaScript-Spezialitäten Ed Post1 ist wohl einer der allgemein anerkanntesten...","categories": ["blog"],
        "tags": ["Metacodes","EdPost","Interview","JavaScript","ProgrammingLanguage","GeneralPurposeLanguage","Reactive","Threading","WebAssembly","Rant"],
        "url": "https://www.metacodes.pro/blog/ed_post_ueber_javascript_(2)/"
      },{
        "title": "REFCODES.ORG change list version 1.10.3",
        "excerpt":"Change list for REFCODES.ORG artifacts’ version 1.10.3 This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-01-20 at 18:15:24. Change list &lt;refcodes-licensing&gt; (version 1.10.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 1.10.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 1.10.3) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 1.10.3) [MODIFIED]...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-1.10.3/"
      },{
        "title": "Ed Post über JavaScript (3/3)",
        "excerpt":"In Teil 1 dieses Interviews haben wir uns mit den wilden Jahren von JavaScript befasst, in Teil 2 sprachen wir über besondere JavaScript-Spezialitäten und im folgenden Teil 3 wagt Ed Post ein Fazit und eine Prognose zu JavaScript. Teil 3: Ein Fazit und eine Prognose Ed Post1 ist wohl einer...","categories": ["blog"],
        "tags": ["Metacodes","EdPost","Interview","JavaScript","ProgrammingLanguage","GeneralPurposeLanguage","Reactive","Threading","WebAssembly","Rant"],
        "url": "https://www.metacodes.pro/blog/ed_post_ueber_javascript_(3)/"
      },{
        "title": "REFCODES.ORG change list version 2.0.0",
        "excerpt":"This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-02-19 at 15:02:01. Change list &lt;refcodes-licensing&gt; (version 2.0.0) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.0.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.0.0) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.0.0) [ADDED] OffsetAccessor.java (see Javadoc at OffsetAccessor.java) [MODIFIED] pom.xml [MODIFIED] ValueAccessor.java...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.0/"
      },{
        "title": "Automatische Testdatengenerierung für Blackbox-Systeme",
        "excerpt":"Testanalysten benötigen für unterschiedlichste Fachanwendungen maßgeschneiderte Testdaten in großen Mengen. Das manuelle Erstellen der Testdaten nebst der zugrunde liegenden Datenstrukturen ist höchst aufwändig. Daher werden die Schnittstellen von den zu testenden Fachanwendungen automatisch analysiert, um daraus das zugrunde liegende Datenmodell für die Testdatengenerierung zu erzeugen. Dabei werden die Testdaten nicht...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/automatische_testdatengenerierung_fuer_blackbox_systeme/"
      },{
        "title": "The mystery of three letter acronyms",
        "excerpt":"Playing around with Three Letter Acronyms (TLA) seems to have some history in computing. You’ll find them in SciFi movies or in the naming of some products. Is it just by chance of what comes out when thinking too much about some TLA or did someone deliberately play around with...","categories": ["blog"],
        "tags": ["Metacodes","ThreeLetterAcronyms","TLA","MOTHER","MUTHUR","HAL","Alien","BillGates","WinNT","WindowsNT","VMS","TWAIN","TOFLA","TAMASLA","HAMAELTA"],
        "url": "https://www.metacodes.pro/blog/the_mystery_of_three_letter_acronyms/"
      },{
        "title": "REFCODES.ORG change list version 2.0.1",
        "excerpt":"This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-04-29 at 06:56:43. REFCODES.ORG archetypes Most notably for this release is the support of Maven Archetypes: Please adjust my.corp with your actual Group-ID and myapp with your actual Artifact-ID: refcodes-console-ext-archetype Use the refcodes-console-ext-archetype to create a...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","Archetype","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.1/"
      },{
        "title": "Coding Serbia 2019 - With swarm intelligence on Burglar Hunt",
        "excerpt":"On the 17th of May if was happy to speak at the Coding Serbia 2019, presenting my topic “With swarm intelligence on Burglar Hunt” (“Internet of Things: Experiences of an Enterprise Developer”). From May 16, 2019 till May 17 the Coding Serbia 2019 took place in Novi Sad (Serbia) this...","categories": ["blog"],
        "tags": ["Refcodes","IoT","SwarmIntelligence","CodingSerbia","CodingSerbia2019","2019","Conference","Speaker","ConferenceSpeaker","NoviSad","Serbia","CaseStudy","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/coding_serbia_2019/"
      },{
        "title": "REFCODES.ORG change list version 2.0.2",
        "excerpt":"This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-06-10 at 17:19:28. Media-Type text/html Most notably for this release is the support of the Media-Type text/html for the CanonicalMap type by the following classes: HtmlCanonicalMapFactorySingleton.java (see Javadoc at HtmlCanonicalMapFactorySingleton.java) HtmlCanonicalMapFactory.java (see Javadoc at HtmlCanonicalMapFactory.java) HtmlMediaTypeFactory.java...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.2/"
      },{
        "title": "Hello IOTCODES.ORG!",
        "excerpt":"aäb cde fgh ijk lmn oöp qrsß tuü vwx yz AÄBC DEF GHI JKL MNO ÖPQ RST UÜV WXYZ !”§ $%&amp; /() =?* ‘&lt;&gt; # ; ²³~ @´ ©«» ¼× {} aäb cde fgh ijk lmn oöp qrsß tuü vwx yz AÄBC DEF GHI JKL MNO ÖPQ RST UÜV WXYZ...","categories": ["iotcodes"],
        "tags": ["IotCodes","SiegfriedSteiner"],
        "url": "https://www.metacodes.pro/iotcodes/hello_iotcodes_org/"
      },{
        "title": "REFCODES.ORG change list version 2.0.3",
        "excerpt":"This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-07-07 at 19:12:52. XML and HTML factories Most notably for this release are the overhauled XML and HTML parsers (supporting marshaling as well as unmarshaling) found in the refcodes-structure-ext-factory artifact as well as in the refcodes-net...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.3/"
      },{
        "title": "REFCODES.ORG change list version 2.0.4",
        "excerpt":"This change list has been auto-generated on triton.local by steiner with changelist-all.sh on the 2019-07-21 at 14:09:42. Change list &lt;refcodes-licensing&gt; (version 2.0.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 2.0.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 2.0.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 2.0.4) [ADDED] BoxGrid.java (see Javadoc...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.4/"
      },{
        "title": "IT-Schulungen: Spring Boot, JEE, ISAQB Foundation",
        "excerpt":"Bei den IT-Schulungen handelt es sich um drei verschiedene Schulungen, die unabhängig voneinander durchgeführt werden und eine Dauer von zwei bis vier Tagen haben. Bei den Schulungen wird den Teilnehmenden sowohl der theoretischen Unterbau zu den jeweiligen Themen erläutert sowie der praktischen Umgang mit den jeweiligen Technologien in Form von...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/it-schulungen_spring_boot_jee_isaqb_foundation/"
      },{
        "title": "REAL - Relational expressed assembly language",
        "excerpt":"WHY WE ARE DOOMED TO STAY PROSPECTORS OR MINERS AND WHY THAT SHOULD NOT BE INEVITABLE - When we create program code, we occasionally dig up algorithms and structures of real elegance and value. Just as a prospector or a miner occasionally digs up gold nuggets, gemstones or even diamonds....","categories": ["papers"],
        "tags": ["Papers","Disruptive","Relational","Assembler","ProgrammingLanguage","SiegfriedSteiner"],
        "url": "https://www.metacodes.pro/papers/REAL/"
      },{
        "title": "Java Profiler sponsored by JProfiler",
        "excerpt":"Thanks to ej-technologies GmbH’s OSS support initiative, REFCODES.ORG now profiles its code with the award winning JProfiler java profiler. I am happy to use this great tool for my open source software - try it out yourself by directly following the link to the  landing page! ","categories": ["blog"],
        "tags": ["Refcodes","JavaProfiler","JProfiler","2019","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/java_profiler_jprofiler/"
      },{
        "title": "refcodes-audio: Handle WAV files and line-out with just some I/O streams",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides audio processing functionality such as generating sine waves or...","categories": ["refcodes"],
        "tags": ["Refcodes","WAV","CSV","SVG","Audio","Sine","Cosine","LineOut","Waves","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-audio/"
      },{
        "title": "REFCODES.ORG change list version 2.0.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2019-12-26 at 14:45:46. Detangled and updated dependencies To keep fat JAR sizes small when using REFCODES.ORG artifacts, some dependency requirements have been resolved in code or have been detangled in the Maven dependencies, whilst updating for...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.0.5/"
      },{
        "title": "Experimenting with sinusoids and overlaying waves",
        "excerpt":"The WAVES experimentation box is a command line tool for generating and processing sinusoid audio data, immediately playing the result to your computer’s “audio out” line or saving it into WAV or CSV files. Harnessing your shell’s pipe and filter mechanism you can apply freely succeeding processing steps. This tool...","categories": ["funcodes"],
        "tags": ["Funcodes","WAV","CSV","SVG","Audio","Sine","Cosine","LineOut","Java","Shell","PipesAndFilters","RefcodesAudio","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/funcodes/experimenting_with_sinusoids_and_overlaying_waves/"
      },{
        "title": "refcodes-tabular: Process tabular data and CSV files using POJOs",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This archetype helps processing table like data structures including the processing of...","categories": ["refcodes"],
        "tags": ["Refcodes","Tabular","CSV","Records","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-tabular/"
      },{
        "title": "Research Microframeworks für Superlightweight Microservices in der Cloud",
        "excerpt":"Klassische JEE auf der einen Seite ist nicht für die Cloud geeignet, aber weithin bekannt und erprobt. Microservices und FaaS auf der anderen Seite sind leichtgewichtige und wenig komplexe Dienste in der Cloud. JEE ist gar nicht und Spring Boot ist bedingt auf schnelle Startup-Zeiten und kleinen Footprint ausgelegt. Microfraeworks...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/research_microframeworks_fuer_superlightweight_microservices_in_der_cloud/"
      },{
        "title": "Scrolling and fading video conferencing with PixGRID",
        "excerpt":"The year 2020 was the year of video conferencing and remote meetings (“corona”) … how can you ease the situation of your colleagues when the meetings are getting boring? You can use PixGRID as an unintrusive moving background for your home television studio. So when it becomes boring for your...","categories": ["blog","funcodes"],
        "tags": ["Metacodes","Funcodes","Refcodes","2020","VideoConferencing","VideoMeetings","PixGRID","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/funcodes/scrolling_and_fading_video_conferencing/"
      },{
        "title": "Beratung zum Umbau eines Monolithen",
        "excerpt":"Unter Berücksichtigung diverser Einschränkungen (technische Gegebenheiten, organisatorische Möglichkeiten) sowie dem Aufbau des Teams wurde ein Vorgehen gemeinsam mit dem Team erarbeitet, um Schwierigkeiten bei der Weiterentwicklung eines monolithisch aufgebauten Softwaresystems zu begegnen. Als Ergebnis entstand eine Strategie zur Erreichung kurzfristiger Ziele (Modularisierung, Testbarkeit, Performance) sowie langfristiger Ziele (Microservices, CI/CD) sowie...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/beratung_zum_umbau_eines_monolithen/"
      },{
        "title": "REFCODES.ORG change list version 2.1.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-01-02 at 19:54:48. Artifacts name changes To be more concise, some artifacts’ names were changed, please update your pom.xml files accordingly when upgrading to version 2.1.0: Old artifact name New artifact name refcodes-console refcodes-cli refcodes-net refcodes-web...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.0/"
      },{
        "title": "Using TTY2MQTT to bridge between serial communication and MQTT",
        "excerpt":"The TTY2MQTT command line tool is a bridge between TTY (COM) serial communication and an MQTT message broker and provides a CLI for publishing and subscribing data from and to a serial port from and to an MQTT message broker. You can download TTY2MQTT from this site’s downloads section. You...","categories": ["funcodes"],
        "tags": ["Refcodes","Funcodes","Java","MQTT","MessageBroker","SerialCommunication","Serialization","TTY","COM","ComPort","Maven","BareMetal","Serverless","IoT","InternetOfThings","Cloud","CloudComputing"],
        "url": "https://www.metacodes.pro/funcodes/using_tty2mqtt_to_bridge_between_serial_communication_and_mqtt/"
      },{
        "title": "EAI zur Automatisierung von Prozessen im öffentlichen Dienst",
        "excerpt":"Verarbeitungsprozesse für von Bürgern genutzte Dienstleistungen werden von Sachbearbeitern bzw. Sachbearbeiterinnen meist manuell mit unterschiedlichen Softwaresystemen begleitet. Zur Automatisierung dieser Verarbeitungsprozesse gilt es, EAI-Komponenten zu entwickeln, die eine einheitliche Web-Schnittstelle mit den heterogenen Softwaresystemen im Hintergrund verbinden. Thema Integration (EAI) unterschiedlicher Prozesse und Softwaresysteme Branche Öffentlicher Dienst Rolle SW-Architekt, SW-Entwickler...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/eai_zur_automatisierung_von_prozessen_im_oeffentlichen_dienst/"
      },{
        "title": "REFCODES.ORG change list version 2.1.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-02-11 at 20:18:53. REFCODES.ORG archetypes The Maven Archetypes are now consolidated in the refcodes-archetype-alt artifact, therefore invocation of the according Maven Archetypes has changed: Please adjust my.corp with your actual Group-ID and myapp with your actual...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.1/"
      },{
        "title": "REFCODES.ORG change list version 2.1.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-07-23 at 22:15:27. Eat your own dog food This release is all about “Eat your own dog food”: All experiences with the developmentof the applications below the downloads area and customer projectsflowed back into the REFCODES.ORG...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.2/"
      },{
        "title": "Manpages online!",
        "excerpt":"The manpages for the FUNCODES.CLUB commands are online now: Quite a few commandline tools related to various of my METACODES.PRO projects have emergedin the past few years. The last few days I spent unifying the command line interfacesof all those tools and decided to create manpages for each of themfor...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Java","Maven","BareMetal","Serverless","SiegfriedSteiner"],
        "url": "https://www.metacodes.pro/manpages/manpages_online/"
      },{
        "title": "CHAOS manpage",
        "excerpt":"CHAOS(1) General Commands Manual NAME chaos — Encryption and decryption tool of file or stream data using Chaos-based encryption. SYNOPSIS chaos -e { -t &lt;text&gt; | -b &lt;bytes&gt; } [ { –hex | –base64 | –encoding &lt;encoding&gt; } ] [ { [ { –prompt | -p &lt;password&gt; | –context &lt;context&gt;...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Chaos","ChaosChaos","GeneralCommands","RefcodesSecurity","Security","Cryptography","Encrypt","Decrypt","ChaosBasedCryptography","ChaosBasedEncryption","PoincaréFunction","Algorithm","BareMetal","Serverless","Java","Maven","SönkeRehder"],
        "url": "https://www.metacodes.pro/manpages/chaos_manpage/"
      },{
        "title": "PICDAT manpage",
        "excerpt":"PICDAT(1) General Commands Manual NAME picdat — Tool for converting (raw) data to a pixmap image (“[pic]ture[dat]a”) and extracting raw (pixmap) data from an image. SYNOPSIS picdat { -c --width &lt;width&gt; --height &lt;height&gt; [ --color-depth &lt;colorDepth&gt; ] [ --offset &lt;offset&gt; ] | -e } -i &lt;inputFile&gt; -o &lt;outputFile&gt; [ -v...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Picdat","PicdatCommand","GeneralCommands","Pixmap","Bitmap","Image","Extract","Convert","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/picdat_manpage/"
      },{
        "title": "DADUM manpage",
        "excerpt":"DADUM(1) General Commands Manual NAME dadum — Tool for dumping mass data (“[da]ta-[dum]per”) by pattern to files or streams. SYNOPSIS dadum -s &lt;size&gt; [ { --rnd-bytes | --rnd-text [ --rnd-text-mode &lt;rndTextMode&gt; ] | -b &lt;bytes&gt; | -t &lt;text&gt; } ] [ -o &lt;outputFile&gt; [ -v ] ] [ --unit &lt;sizeUnit&gt;...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Dadum","DadumCommand","GeneralCommands","Data","RawData","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/dadum_manpage/"
      },{
        "title": "WAVES manpage",
        "excerpt":"WAVES(1) General Commands Manual NAME waves — Tool to generate or pipe and filter (sound) waves for audio playback and export. SYNOPSIS waves -f &lt;frequencyHz&gt; -c &lt;curveFunction&gt; [ -a &lt;amplitude&gt; ] [ -x &lt;xOffset&gt; ] [ -y &lt;yOffset&gt; ] [ -v ] [ { [ -l &lt;lengthSec&gt; ] [ -s...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","WAV","CSV","SVG","Audio","Sine","Cosine","LineOut","Waves","UserManual","ManPage","ManualPage","Command","GeneralCommands","WavesCommand","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/waves_manpage/"
      },{
        "title": "TTY2MQTT manpage",
        "excerpt":"TTY2MQTT(1) General Commands Manual NAME tty2mqtt — Tool bridging between a serial port and an MQTT message broker for publishing or subscribing messages. SYNOPSIS tty2mqtt ∅ | [ --config &lt;config&gt; ] [ -p &lt;tty/port&gt; ] [ -b &lt;tty/baud&gt; ] [ --data-bits &lt;tty/dataBits&gt; ] [ --stop-bits &lt;tty/stopBits&gt; ] [ --parity &lt;tty/parity&gt;...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Tty2mqtt","Tty2mqttCommand","GeneralCommands","MQTT","MessageBroker","SerialCommunication","Serialization","TTY","COM","ComPort","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/tty2mqtt_manpage/"
      },{
        "title": "PIXGRID manpage",
        "excerpt":"PIXGRID(1) General Commands Manual NAME pixgrid — Tool for displaying a light bulb matrix rendering configurable scrolling and fading pixmaps (PNG, GIF or JPG) SYNOPSIS pixgrid ∅ | [ --config &lt;config&gt; ] [ -q ] [ -d ] pixgrid --init [ --config &lt;config&gt; ] [ -q ] pixgrid -h |...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Tty2mqtt","Tty2mqttCommand","GeneralCommands","MQTT","MessageBroker","SerialCommunication","Serialization","TTY","COM","ComPort","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/pixgrid_manpage/"
      },{
        "title": "WATCHDOG manpage",
        "excerpt":"WATCHDOG(1) General Commands Manual NAME watchdog — Cellular automaton simulating a watchdog in a house using JavaFx. SYNOPSIS watchdog -l [ -v ] [ -d ] watchdog [ -n &lt;neighbourhood&gt; ] [ --board &lt;board&gt; ] [ --field-width &lt;fieldWidth&gt; ] [ --field-height &lt;fieldHeight&gt; ] [ --field-gap &lt;fieldGap&gt; ] [ --console ]...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","GeneralCommands","WatchdogCommand","BoulderDash","Watchdog","CellularAutomaton","SwarmmIntelligence","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/watchdog_manpage/"
      },{
        "title": "AUTOCHAT manpage",
        "excerpt":"AUTOCHAT(1) General Commands Manual NAME autochat — ELIZA chatbot (by Joseph Weizenbaum) talking to you via RESTful services and an MS-Teams endpoint or on the console. SYNOPSIS autochat ∅ | [ -p &lt;port&gt; ] [ -b &lt;basePath&gt; ] [ -q ] [ --config &lt;config&gt; ] [ -d ] autochat -i...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","GeneralCommands","Eliza","JosephWeizenbaum","MsTeams","REST","Restful","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/autochat_manpage/"
      },{
        "title": "HTTPECHO manpage",
        "excerpt":"HTTPECHO(1) General Commands Manual NAME httpecho — Small HTTP-Echo server echoing incoming HTTP-Requests back to the client and to the console. SYNOPSIS httpecho [ -p &lt;port&gt; ] [ -c &lt;connections&gt; ] [ -D ] [ -e ] [ -P ] [ -Q ] [ -H ] [ -B ] [...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","HttpEcho","HttpEchoCommand","GeneralCommands","REST","Restful","Data","RawData","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/httpecho_manpage/"
      },{
        "title": "TELETYPE manpage",
        "excerpt":"TELETYPE(1) General Commands Manual NAME teletype — Teleprinter command line tool for sending data to and receiving data from TTY (serial) ports simultaneously. SYNOPSIS teletype -l [ -q ] [ -d ] teletype -p &lt;port&gt; { -L | -S -m &lt;message&gt; } [ -b &lt;baud&gt; ] [ --data-bits &lt;dataBits&gt; ]...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","TeleType","TeleTypeCommand","GeneralCommands","Data","RawData","SerialCommunication","Serialization","TTY","COM","ComPort","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/teletype_manpage/"
      },{
        "title": "Chaos-based encryption - revisited!",
        "excerpt":"In the blog post Chaos-based encryption I introduced a chaotic cryptology algorithm1 using a Poincaré successor ordinalfunction. In this article I want to present you a command line suite providing this very Chaos-based encryption functionality alongside some extensionsas well as comparison to the AES algorithm. This chaos-based cryptography suite is...","categories": ["blog"],
        "tags": ["Refcodes","RefcodesSecurity","Security","Cryptography","Encrypt","Decrypt","Chaos","ChaosBasedCryptography","ChaosBasedEncryption","PoincaréFunction","Algorithm","BareMetal","Serverless","Java","Maven","SönkeRehder"],
        "url": "https://www.metacodes.pro/blog/chaos-based_encryption_revisited/"
      },{
        "title": "BIN2TXT manpage",
        "excerpt":"BIN2TXT(1) General Commands Manual NAME bin2txt — Encoding and decoding tool for BASE64 and related encodings/decodings from/to ASCII to/from binary. SYNOPSIS bin2txt { -e | -d } [ { -t &lt;text&gt; | -b &lt;bytes&gt; } [ --hex ] [ --encoding &lt;encoding&gt; ] [ -v ] [ --debug ] ] bin2txt...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Bin2Txt","Bin2TxtCommand","GeneralCommands","Base64","Encode","Decode","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/bin2txt_manpage/"
      },{
        "title": "ASCART manpage",
        "excerpt":"ASCART(1) General Commands Manual NAME ascart — A command line tool for creating ASCII art (“[ASC]II[ART]”) from plain text or image files. SYNOPSIS ascart -t &lt;text&gt; [ --font-family &lt;fontFamily&gt; ] [ --font-style &lt;fontStyle&gt; ] [ --color-palette &lt;colorPalette&gt; ] [ --padding &lt;padding&gt; ] [ --border-style &lt;borderStyle&gt; ] [ -o &lt;outputFile&gt; ]...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","Ascart","AscartCommand","GeneralCommands","ASCII","AsciiArt","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/ascart_manpage/"
      },{
        "title": "REFCODES.ORG change list version 2.1.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-08-29 at 21:57:06. Change list &lt;refcodes-licensing&gt; (version 2.1.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.1.3) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.1.3) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.1.3) [MODIFIED] pom.xml [MODIFIED] EncodedAccessor.java (see Javadoc at EncodedAccessor.java)Change list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.3/"
      },{
        "title": "REFCODES.ORG change list version 2.1.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-08-30 at 12:30:45. Sonatype Lift has reviewed thedependencies of this release with no threats being reported. In additon, hardeningof this release was supported by the Open Source Insights. Change list &lt;refcodes-licensing&gt; (version 2.1.4) [MODIFIED] pom.xmlChange list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.4/"
      },{
        "title": "CRACKZIP manpage",
        "excerpt":"CRACKZIP(1) General Commands Manual NAME crackzip — Recovery tool for cracking password protected ZIP (*.zip) files. SYNOPSIS crackzip -b -i &lt;zipFile&gt; [ -o &lt;outputDir&gt; ] [ { -e &lt;expression[…]&gt; | [ --min-length &lt;minLength&gt; ] [ --max-length &lt;maxLength&gt; ] [ { --char-set &lt;charSet&gt; | -a &lt;alphabet&gt; } ] [ -r &lt;resumeFrom&gt;...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","CrackZIP","CrackZIPCommand","GeneralCommands","ZIP","EncryptedZIP","AES","ZipCrypto","Password","PasswordRecovery","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/crackzip_manpage/"
      },{
        "title": "REFCODES.ORG change list version 2.1.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-11-08 at 19:32:42. Change list &lt;refcodes-licensing&gt; (version 2.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.1.5) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.1.5) [MODIFIED] pom.xml [MODIFIED] BusyAccessor.java (see Javadoc at BusyAccessor.java)Change list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.5/"
      },{
        "title": "REFCODES.ORG change list version 2.1.6",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-11-10 at 15:14:03. Change list &lt;refcodes-licensing&gt; (version 2.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 2.1.6) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.1.6/"
      },{
        "title": "REFCODES.ORG change list version 2.2.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-12-01 at 20:27:50. Change list &lt;refcodes-licensing&gt; (version 2.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.2.0) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.2.0) [MODIFIED] pom.xml [MODIFIED] ReadTimeoutInMsAccessor.java (see Javadoc at ReadTimeoutInMsAccessor.java) [MODIFIED] TimeoutInMsAccessor.java...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.2.0/"
      },{
        "title": "IT-Tage 2021 - No WLAN, no LAN: Wiring IoT installations",
        "excerpt":"In December ‘21, I was happy to have a slot as a speaker at the IT-Tage 2021, presenting my topic “Kein WLAN, kein LAN: IoT-Installationen vernetzen” (“No WLAN, no LAN: Wiring IoT installations”). From December 6, 2021 till December 12 the IT-Tage 2021 took place in Frankfurt this year. I...","categories": ["blog"],
        "tags": ["Refcodes","IoT","P2P","PeerToPeer","Peer2Peer","ITTage","ITTage2021","2021","Conference","Speaker","ConferenceSpeaker","Frankfurt","Online","Germany","CaseStudy","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/it_tage_2021/"
      },{
        "title": "PLAYLOAD manpage",
        "excerpt":"PLAYLOAD(1) General Commands Manual NAME playload — Peer-to-Peer (P2P) command line transport system for exchanging messages between participating parties (peers) over serial TTY (COM) ports. SYNOPSIS playload -l [-q ] [-d ] playload -a [-L &lt;locator&gt; ] [--config &lt;config&gt; ] [ {-p &lt;peers/ports[…]&gt; | --pattern &lt;peers/ports/pattern&gt; [--count &lt;peers/ports/count&gt; ] }...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","PlayLoad","PlayLoadCommand","GeneralCommands","P2P","PeerToPeer","Peer2Peer","SerialCommunication","Serialization","TTY","COM","ComPort","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/playload_manpage/"
      },{
        "title": "REFCODES.ORG change list version 2.2.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2021-12-22 at 11:51:53. Change list &lt;refcodes-forwardsecrecy-alt&gt; (version 2.2.1) [DELETED] BasicForwardSecrecyFileSystemTest.java [DELETED] ForwardSecrecyFileSystemTest.java [MODIFIED] pom.xml [MODIFIED] pom.xml [MODIFIED] FileSystemDecryptionServer.java (see Javadoc at FileSystemDecryptionServer.java) [MODIFIED] FileSystemEncryptionServer.java (see Javadoc at FileSystemEncryptionServer.java)Change list &lt;refcodes-io-ext&gt; (version 2.2.1) [MODIFIED] pom.xml [MODIFIED] pom.xmlChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.2.1/"
      },{
        "title": "REFCODES.ORG change list version 2.2.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-01-01 at 02:02:02. Change list &lt;refcodes-licensing&gt; (version 2.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 2.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-time&gt; (version 2.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 2.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-data&gt; (version 2.2.2) [MODIFIED] pom.xmlChange list &lt;refcodes-exception&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-2.2.2/"
      },{
        "title": "JUG Munich - Automatically answer Teams chats with Java microservices",
        "excerpt":"In January 2022 we were offered the opportunity to talk about automatically answering MS-Teams chats with Java microservices at the JUG (Java User Group) Munich (“Teams-Chats mit Java-Microservices automatisch beantworten”) which we gratefully accepted. Classic JEE is not at all and Spring Boot is conditionally designed for fast startup times...","categories": ["blog"],
        "tags": ["Refcodes","MicroFrameworks","MicroServices","FunctionAsAService","FaaS","JUG","JavaUserGroup","2022","Meetup","Speaker","MeetupSpeaker","Munich","Online","Germany","CaseStudy","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/jug_munich_2022/"
      },{
        "title": "refcodes-serial: A serial communication toolkit for Java",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This toolkit targets at programming serial communication at a high abstraction level,...","categories": ["refcodes"],
        "tags": ["Refcodes","Serial","Serialization","TTY","SerialPort","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-serial/"
      },{
        "title": "refcodes-matcher: Build custom matching logic and match path patterns",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact allows the construction of matchers for building arbitrary complex matching...","categories": ["refcodes"],
        "tags": ["Refcodes","Matcher","PathMatcher","Wildcard","PathVariable","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-matcher/"
      },{
        "title": "REFCODES.ORG change list version 3.0.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-04-07 at 17:06:27. Record type support The Record type as of Java 16is now supported throughout the REFCODES.ORG toolkit wherever a type’s instanceto be introspected is accepted as argument or whenever an arbitrary type is to...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.0/"
      },{
        "title": "refcodes-archetype: Using the REFCODES.ORG toolkit made easy",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact hosts several REFCODES.ORG related Maven Archetypes for quickly getting up...","categories": ["refcodes"],
        "tags": ["Refcodes","Archetype","CLI","EventBus","C2","CommandAndControl","REST","RESTful","CSV","IoC","InversionOfControl","DependencyInjection","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-archetype/"
      },{
        "title": "refcodes-generator: Mass produce password permutations and distinct IDs",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides word morphs mass production functionality for generating arbitrary distinct...","categories": ["refcodes"],
        "tags": ["Refcodes","Generator","ID","UUID","UniversallyUniqueIdentifier","Permutation","Password","PasswordRecovery","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-generator/"
      },{
        "title": "REFCODES.ORG change list version 3.0.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-05-31 at 18:33:25. Change list &lt;refcodes-licensing&gt; (version 3.0.1) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] module-info.java (see Javadoc at module-info.java) [MODIFIED] License.java (see Javadoc at License.java) [MODIFIED] package-info.java [MODIFIED] Apache [MODIFIED] GNU [MODIFIED] GPL [MODIFIED] REFCODESChange list &lt;refcodes-parent&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.1/"
      },{
        "title": "REFCODES.ORG change list version 3.0.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-06-03 at 12:01:30. Change list &lt;refcodes-licensing&gt; (version 3.0.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.0.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.0.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.2/"
      },{
        "title": "REFCODES.ORG change list version 3.0.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-06-20 at 19:45:22. Change list &lt;refcodes-licensing&gt; (version 3.0.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.0.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.0.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.3/"
      },{
        "title": "refcodes-criteria: Criteria construction for selecting, filtering or identifying",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact allows the construction of Criteria instances for selecting, filtering or...","categories": ["refcodes"],
        "tags": ["Refcodes","Matcher","PathMatcher","Wildcard","PathVariable","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-criteria/"
      },{
        "title": "REFCODES.ORG change list version 3.0.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-06-24 at 21:59:17. Change list &lt;refcodes-licensing&gt; (version 3.0.4) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.0.4) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] Schema.java (see Javadoc at Schema.java)Change list &lt;refcodes-data&gt; (version 3.0.4)...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.4/"
      },{
        "title": "Agile Vorgehensweisen, SW-Architektur und Projektmanagement",
        "excerpt":"Einführen von (agilen) Vorgehensweisen und Projektmanagement- wie auch SW-Entwurfsmethoden als auch notwendige Moderation und Mediation, Abbau von technischen Schulden, Architekturvereinfachungen, Software-Engineering/Programmierung nebst notwendiger Architektur-Vereinfachung. Thema Aufsetzen eines Scrum-Prozesses und Vereinfachung der SW-Architektur Branche Startup im Medizinsektor Rolle Teamcoach, SW-Architekt Dauer 07.2022 – 12.2022 Tätigkeiten Prozessaufbau (Scrum), Architekturreview, SW-Entwurf, Abstimmung, Moderation...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/agile_vorgehensweisen_sw-architektur_und_projektmanagement/"
      },{
        "title": "PASSGEN manpage",
        "excerpt":"PASSGEN(1) General Commands Manual NAME passgen — A tool for generating password lists written into a file or printed to &lt;STDOUT&gt; by providing rule expressions. SYNOPSIS passgen { -e &lt;expression[…]&gt; | [ --min-length &lt;minLength&gt; ] [ --max-length &lt;maxLength&gt; ] [ { --char-set &lt;charSet&gt; | -a &lt;alphabet&gt; } ] } [...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","PassGen","PassGenCommand","GeneralCommands","Password","Permutations","Generator","PasswordPermutations","PasswordGenerator","PasswordRecovery","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/passgen_manpage/"
      },{
        "title": "Buchtipp: Spring for GraphQL",
        "excerpt":"Reckoning the book Spring for GraphQL from Michael Schäfer very helpful, the Javamagazin 8/22 published an according book review I wrote. Recommended reading Full review in the Javamagazin, issue 8/22, page 8: “Anyone who has had to make friends with ad hoc interfaces in enterprise applications in the past will...","categories": ["blog"],
        "tags": ["Javamagazin","Buchtipp","BookTip","Buch","Book","Java","GraphQL","SpringForGraphQL","MichaelSchäfer"],
        "url": "https://www.metacodes.pro/blog/buchtipp_spring_for_graphql/"
      },{
        "title": "REFCODES.ORG change list version 3.0.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-09-11 at 14:55:58. Change list &lt;refcodes-licensing&gt; (version 3.0.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.5) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.0.5) [MODIFIED] pom.xmlChange list &lt;refcodes-mixin&gt; (version 3.0.5) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] ResultAccessor.java (see Javadoc...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.5/"
      },{
        "title": "REFCODES.ORG change list version 3.0.6",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-09-25 at 14:25:29. Support for SLF4J version &gt;= 2 As of SLF4J being available in version 2 now, you can use the existing SLF4J binding refcodes-logger-alt-slf4j and make your refcodes-logger log via SLF4J (of version &gt;=...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","Logger","RefcodesLogger","SLF4J","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.6/"
      },{
        "title": "FLATML manpage",
        "excerpt":"flatml(1) General Commands Manual NAME flatml — A shell filter flattening markup properties of XML, JSON, YAML, INI or PROPERTIES format to a key/value per line format for easy addressing elements in complex data sets. SYNOPSIS flatml [ -i &lt;inputFile[…]&gt; ] [ -o &lt;outputFile&gt; ] [ { -f &lt;format&gt; [...","categories": ["manpages"],
        "tags": ["Metacodes","Funcodes","Refcodes","UserManual","ManPage","ManualPage","Command","FlatML","FlattenMarkup","Merge","XML","JSON","YAML","INI","PROPERTIES","XML2","GeneralCommands","BareMetal","Serverless","Java","Maven"],
        "url": "https://www.metacodes.pro/manpages/flatml_manpage/"
      },{
        "title": "Java deliverables as executable bundle and the launcher trick",
        "excerpt":"In the past I coded some command line tools in Java which are quite useful for me and which I ought to be useful for others1. Providing just fat JARfiles implies launching of those Java applications to be more or less cumbersome: Besides requiring a JVM being installed, some annoying...","categories": ["blog"],
        "tags": ["Refcodes","Java","Launcher","Bundle","CommandLineInterface","CLI","CommandLine","Command","Bash","Console","Terminal","GunLinux","Linux","MsWindows","MicrosoftWindows","Windows","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/java_deliverables_as_executable_bundle_and_the_launcher_trick/"
      },{
        "title": "REFCODES.ORG change list version 3.0.7",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-11-05 at 11:56:58. Change list &lt;refcodes-licensing&gt; (version 3.0.7) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.0.7) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] DateFormatTest.javaChange list &lt;refcodes-mixin&gt; (version 3.0.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.7/"
      },{
        "title": "GraalVM: Native command line tools for Linux and Windows written in Java",
        "excerpt":"Thanks to the GraalVM, you may compile your Java command line tools1 to native executables, directly running on MS Windows or GNU Linux without any JVM being installed or bundled. As a side effect, the resulting *.exe and *.elf files start up lightening fast, which makes your commands feasible drop...","categories": ["blog"],
        "tags": ["Refcodes","Java","GraalVM","CommandLineInterface","CLI","CommandLine","Command","Bash","Console","Terminal","NativeImage","GunLinux","Linux","MsWindows","MicrosoftWindows","Windows","EXE","ELF","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/blog/graalvm_native_command_line_tools_for_linux_and_windows_written_in_java/"
      },{
        "title": "refcodes-io: Paving the road for complex and low level I/O across boundaries",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact defines basic types handling communication between processes (across system boundaries),...","categories": ["refcodes"],
        "tags": ["Refcodes","IO","InputOutput","Input","Output","InputStream","OutputStream","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-io/"
      },{
        "title": "REFCODES.ORG change list version 3.0.8",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2022-12-01 at 13:08:12. Generic CLI short and long options Most notably are the changes applied to the refcodes-cli artifact by decoupling the long and the short option prefixes from your syntax definition and by replacing the...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.8/"
      },{
        "title": "Hardware und Software zur Unterstützung des Klavierspiels mit IoT",
        "excerpt":"Ein Mikrocontroller von Typ RaspberryPi steuert über einen Hardware-Adapter eine eigens dafür entworfene LED-Leiste, deren LEDs in ihren Anordnungen den Tasten einer Klaviatur entsprechen. Eine spezielle Software konvertiert vorhandenes Notenmaterial in ein geeignetes Format, um über eine spezielle Logik auf Seiten des RaspberryPi auf die LEDs der LED-Leiste abgebildet zu...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/hardware_und_software_zur_unterst%C3%BCtzung_des_klavierspiels_mit_iot/"
      },{
        "title": "REFCODES.ORG change list version 3.0.9",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-01-06 at 15:27:28. Change list &lt;refcodes-licensing&gt; (version 3.0.9) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.0.9) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.0.9) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.0.9) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] AbstractSchema.java...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.0.9/"
      },{
        "title": "Write once, run anywhere: An Android game using JavaFX and the GraalVM",
        "excerpt":"Get it into the Google Play Store! Lately I was reasoning on how to provide Java applications as launchers, bundles or installers1 as well as native executables2 . Now I go for an Android game programmed using Java &gt;= 16 and JavaFX which runs besides on Android smartphones also on...","categories": ["blog"],
        "tags": ["Refcodes","Comcodes","Android","App","Game","BoulderDash","GraalVM","JavaFX","Java","Maven"],
        "url": "https://www.metacodes.pro/blog/write_one_run_anywhere_an_android_game_using_javafx_and_the_graalvm/"
      },{
        "title": "IT-Beratung: Aufteilen eines Monolithen in Module",
        "excerpt":"Es gilt den Ist-Stand eines Monolithen zur Simulation von komplexen Zusammenhängen zu analysieren und eine Zielarchitektur sowie das Vorgehen (um diesen Architekturumbau hin zur Zielarchitektur zu bewältigen) gemeinsam mit dem Lead-Entwickler zu erarbeiten. Dabei gilt es auch, kontinuierlich die Architekturentscheidungen und das Vorgehen zu kommunizieren. Auch generelle IT-Beratung z.B. zu...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/it-beratung_aufteilen_eines_monolithen_in_module/"
      },{
        "title": "Java Software Developer and IoT Programmer: Your Coding Architect",
        "excerpt":"As a freelance software developer with expertise in Java, Spring Boot and Maven (among others) as well as experience in programming for IoT uses cases using PlatformIO and C, I can bring a unique set of skills to your software development projects. Whether you need someone to help you design...","categories": ["blog"],
        "tags": ["Java","Maven","SpringBoot","Android","GraalVM","JavaFX","PlatformIO","C","Linux","Bash","Docker","SoftwareArchitecture","Programming","Training","OSS","OpenSourceSoftware","Refcodes","Comcodes","Funcodes"],
        "url": "https://www.metacodes.pro/blog/java_software_developer_and_iot_programmer/"
      },{
        "title": "REFCODES.ORG change list version 3.1.0",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-03-01 at 17:17:30. Maintenance release This is a maintenance release preparing the next big thing to come. Mainly all dependency versions as well as plugin versions have been lifted and all related projects have been adjusted....","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.1.0/"
      },{
        "title": "Yet another Big-O cheat sheet",
        "excerpt":"Big-O notation is used in computer science to describe the performance or efficiencyof algorithms in terms of their growth rate related to the input size. It describeshow the number of operations for an algorithm gets bigger as the number of processedelements increases, providing a way to analyze and compare the...","categories": ["blog"],
        "tags": ["Metacodes","Linux","BigO","Complexity","Algorithms","CheatSheet"],
        "url": "https://www.metacodes.pro/blog/yet_another_big_o_cheat_sheet/"
      },{
        "title": "LEDIANO, Bespielen eines Piano-LED-Streifens",
        "excerpt":"LEDIANO ist eine Software, die über einen Raspberry Pi eine LED-Leiste steuert, um die Koordination von Motorik und Gehör zu fördern, indem die zu spielenden Klaviertasten durch das Aufleuchten der entsprechenden LEDs angezeigt werden. Bei diesem Projekt wurde der LED-Streifen in einem ersten Schritt mit einem Arduino über die serielle...","categories": ["blog"],
        "tags": ["Comcodes","LED","LedStripe","Piano","Keyboard","Arduino","RasperryPi","COM","TTY","Android","App","Java","Maven"],
        "url": "https://www.metacodes.pro/blog/lediano_bespielen_eines_piano_led-streifens/"
      },{
        "title": "REFCODES.ORG change list version 3.1.1",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-04-16 at 20:06:59. New REFCODES.ORG archetypes The refcodes-archetype-alt artifact got an offspring archetype: Please adjust my.corp with your actual Group-ID and myapp with your actual Artifact-ID: refcodes-archetype-alt-decoupling Use the refcodes-archetype-alt-decoupling archetype to create a bare metal...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.1.1/"
      },{
        "title": "REFCODES.ORG change list version 3.1.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-04-21 at 17:55:58. Corrected module names The following erroneous module names from release 3.1.1 have been corrected in this release: refcodes-properties-ext-runtime → refcodes-properties-ext-application refcodes-decoupling-ext-runtime → refcodes-decoupling-ext-applicationNew REFCODES.ORG archetype The refcodes-archetype-alt artifact got an offspring archetype: Please...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.1.2/"
      },{
        "title": "refcodes-decoupling: Breaking up dependencies between components",
        "excerpt":"README The REFCODES.ORG codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers. What is this repository for? This artifact provides a reactor for breaking up dependencies between a Java...","categories": ["refcodes"],
        "tags": ["Refcodes","InversionOfControl","IoC","DependencyInjection","Decoupling","Java","Maven","BareMetal","Serverless"],
        "url": "https://www.metacodes.pro/refcodes/refcodes-decoupling/"
      },{
        "title": "REFCODES.ORG change list version 3.1.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-04-30 at 14:39:59. Fewer *Impl files During the process of consolidation, some *Impl classes lost their Impl suffix: HttpRestClientImpl → HttpRestClient HttpRestServerImpl → HttpRestServer LoopbackRestClientImpl → LoopbackRestClient LoopbackRestServerImpl → LoopbackRestServer EurekaRestClientImpl → EurekaRestClient EurekaRestServerImpl → EurekaRestServerChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.1.3/"
      },{
        "title": "REFCODES.ORG change list version 3.2.2",
        "excerpt":"Please ignore the releases 3.2.0, 3.2.1, 3.2.2 and 3.2.3 as of some release hickups resulting in some artifacts still remaining in zombie status between Sonatype Staging and final release on Maven Central. This change list has been auto-generated on phoenix by steiner with changelist-all.sh on the 2023-05-28 at 18:29:00. Change...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.2/"
      },{
        "title": "REFCODES.ORG change list version 3.2.3",
        "excerpt":"Please ignore the releases 3.2.0, 3.2.1, 3.2.2 and 3.2.3 as of some release hickups resulting in some artifacts still remaining in zomby status between Sonatype Staging and final release on Maven Central. This change list has been auto-generated on phoenix by steiner with changelist-all.sh on the 2023-05-29 at 18:26:29. Change...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.3/"
      },{
        "title": "REFCODES.ORG change list version 3.2.4",
        "excerpt":"Please ignore the releases 3.2.0, 3.2.1, 3.2.2 and 3.2.3 as of some release hickups resulting in some artifacts still remaining in zomby status between Sonatype Staging and final release on Maven Central. This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-06-14 at 17:52:00. Change...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.4/"
      },{
        "title": "REFCODES.ORG change list version 3.2.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-06-16 at 13:50:55. Fixes Fixed Issue #6 if the refcodes-cli artefactChange list &lt;refcodes-licensing&gt; (version 3.2.5) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.2.5) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.2.5) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.5/"
      },{
        "title": "REFCODES.ORG change list version 3.2.6",
        "excerpt":"In addition to new functionality, this release implements a guard to prevent crashing of the third party library jansi (as of JVM crashes due to systemInstall() when running javaw (no terminal). This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-06-23 at 16:58:05. Change list...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.6/"
      },{
        "title": "REFCODES.ORG change list version 3.2.7",
        "excerpt":"This release is mainly dedicated to code maintenance and the application of best practices. In this process, tests were standardized and code maintenance tools were applied. Robustness has been improved by special treatment of rare defects. This change list has been auto-generated on triton by steiner with changelist-all.sh on the...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.7/"
      },{
        "title": "REFCODES.ORG change list version 3.2.8",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-08-20 at 14:36:14. Change list &lt;refcodes-licensing&gt; (version 3.2.8) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.2.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.2.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.2.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.2.8/"
      },{
        "title": "Intensiv-Schulung Spring Boot JPA, Themenblock Kafka",
        "excerpt":"Zusätzlich zu den regelmäßig gehaltenen IT-Schulungen gesellt sich eine Intensiv-Schulung zum Thema Spring Boot mit Datenbanken hinzu, bei der die Themen JPA, JPQL, O/R-Mapper, Hibernate/EclipseLink, das Transaction-Management sowie das Exception-Handling eine besondere Rolle spielen und auf die zugrundeliegende Architektur nebst JDBC eingegangen wird. Darüber Hinaus wurde die klassische Spring Boot...","categories": ["projects"],
        "tags": ["Profile","Projects"],
        "url": "https://www.metacodes.pro/projects/schulungs_update_jpa_und_kafka/"
      },{
        "title": "REFCODES.ORG change list version 3.3.1",
        "excerpt":"Please ignore the releases 3.2.9 and 3.3.0 as of some release hickups resulting in some artifacts still remaining in zombie status between Sonatype Staging and final release on Maven Central. This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-09-17 at 17:49:48. Change list &lt;refcodes-licensing&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.1/"
      },{
        "title": "REFCODES.ORG change list version 3.3.2",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-09-28 at 09:25:32. Change list &lt;refcodes-licensing&gt; (version 3.3.2) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.2) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.2/"
      },{
        "title": "REFCODES.ORG change list version 3.3.3",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-10-23 at 15:19:40. Change list &lt;refcodes-licensing&gt; (version 3.3.3) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.3) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.3) [ADDED] Bytes.java (see Javadoc at Bytes.java)...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.3/"
      },{
        "title": "REFCODES.ORG change list version 3.3.4",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2023-11-22 at 16:19:36. Change list &lt;refcodes-licensing&gt; (version 3.3.4) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.4) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.4/"
      },{
        "title": "Yet another VI(M) cheat sheet",
        "excerpt":"As I tend to forget how to invoke the most essential vi functionality I am in the desperate need of a vi/vim cheat sheet, which is why I stumbled across the vi Editor “Cheat Sheet” from the University at Albany, that, with just very few additions, fits my needs for...","categories": ["blog"],
        "tags": ["Metacodes","vi","vim","CheatSheet"],
        "url": "https://www.metacodes.pro/blog/yet_another_vim_cheat/"
      },{
        "title": "Yet another `AI` cheat sheet",
        "excerpt":"As AI (Artificial Intelligence) continues to evolve, I sat down to prepare a structured overview of this topic for me to the have some cheat sheet for looking up related terms and definitions quickly. Of course, as is appropriate for this topic, it is important to note that I wrote...","categories": ["blog"],
        "tags": ["Metacodes","AI","ArtificialIntelligence","ML","MachineLearning","CheatSheet"],
        "url": "https://www.metacodes.pro/blog/yet_another_ai_cheat_sheet/"
      },{
        "title": "Short-Term effects and Long-Term impacts of AI and Machine Learning",
        "excerpt":"Short-Term effects and Long-Term impacts of AI and Machine Learning Reasoning upon the current rise of AI (Artificial Intelligence) and Machine Learning (ML), I scribbled down some thoughts regarding the Short-Term effects and Long-Term impacts of AI and had GPT-4 render my input into a blog post (statements 2. or...","categories": ["blog"],
        "tags": ["Metacodes","AI","ArtificialIntelligence","ML","MachineLearning","Philosophy"],
        "url": "https://www.metacodes.pro/blog/short-term_effects_and_long-term_impacts_of_ai/"
      },{
        "title": "REFCODES.ORG change list version 3.3.5",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2024-02-29 at 20:01:11. Change list &lt;refcodes-licensing&gt; (version 3.3.5) [MODIFIED] .gitignore [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.5) [MODIFIED] .gitignore [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.5) [MODIFIED] .gitignore [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.5)...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.5/"
      },{
        "title": "REFCODES.ORG change list version 3.3.6",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2024-05-21 at 14:58:15. Change list &lt;refcodes-licensing&gt; (version 3.3.6) [MODIFIED] pom.xml [MODIFIED] License.java (see Javadoc at License.java) [MODIFIED] package-info.javaChange list &lt;refcodes-parent&gt; (version 3.3.6) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.6) [MODIFIED] pom.xml [MODIFIED] README.md [MODIFIED] DateFormat.java...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.6/"
      },{
        "title": "REFCODES.ORG change list version 3.3.7",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2024-05-30 at 20:01:00. Change list &lt;refcodes-licensing&gt; (version 3.3.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-parent&gt; (version 3.3.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.7) [MODIFIED] pom.xml [MODIFIED] README.mdChange...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.7/"
      },{
        "title": "REFCODES.ORG change list version 3.3.8",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2024-08-24 at 22:12:39. Change list &lt;refcodes-licensing&gt; (version 3.3.8) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.8) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.8/"
      },{
        "title": "How the ingenious design of BASIC democratized programming for the masses",
        "excerpt":"In the early days of microcomputers, a surprising hero emerged: the BASIC programming language. Despite its later reputation for being overly simplistic or “unprofessional”, BASIC was ingeniously designed for its time. Its technical brilliance allowed millions of people to explore programming, making it a crucial bridge between complex computing and...","categories": ["blog"],
        "tags": ["BASIC","ProgrammingLanguages","HomeComputers","Microcomputers","ComputerArcheology","Kemeny","Kurtz"],
        "url": "https://www.metacodes.pro/blog/how_the_ingenious_design_of_basic_democratized_programming_for_the_masses/"
      },{
        "title": "REFCODES.ORG change list version 3.3.9",
        "excerpt":"This change list has been auto-generated on triton by steiner with changelist-all.sh on the 2024-12-07 at 21:17:17. Change list &lt;refcodes-licensing&gt; (version 3.3.9) [MODIFIED] pom.xmlChange list &lt;refcodes-parent&gt; (version 3.3.9) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-time&gt; (version 3.3.9) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-mixin&gt; (version 3.3.9) [MODIFIED] pom.xml [MODIFIED] README.mdChange list &lt;refcodes-data&gt;...","categories": ["refcodes","changelist"],
        "tags": ["ChangeList","Refcodes","BareMetal","MicroFramework","Serverless","Java","Maven","MavenCentral","Artifact","GPL","ApacheLicense","GnuClasspath","ReleaseNotes"],
        "url": "https://www.metacodes.pro/refcodes/changelist/refcodes.org_change_list_versions-3.3.9/"
      }]
