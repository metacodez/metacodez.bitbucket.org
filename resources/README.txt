=== Rounded corners for the slides ===

find . -type f -name "*.png" -exec convert -size 640x480 xc:none -fill white -draw "roundRectangle 0,0 640,480 45,45" {} -compose SrcIn -composite {} \;

Please note: Adjust the resolution on the both declarations ("640x480") according to the size of the slides.