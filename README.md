# README #

> "The [`METACODES.PRO`](http://www.metacodes.pro) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers."

## What is this repository for? ##

***This repository contains `jekyll` working files being the source of the static `metacodes.bitbucket.org` blog for the [`METACODES.PRO`](http://www.metacodes.pro) OSS projects (or in case of the actual `metacodes.bitbucket.org` web-site the it contains the generated `jekyll` web-site files).***

## How do I get set up? ##

## Ruby

Install ruby in a version like `2.7.6`

### RVM

As ruby in version `2.7.6` is outdated, you may use [`rmv`](https://github.com/rvm/rvm/), the riby version manager:

```
cd ~/Temp
curl -L get.rvm.io > rvm-install
less rvm-install 
chmod +x rvm-install 
./rvm-install --auto-dotfiles
rvm pkg install openssl
```

Usage of ruby 2.7:

```
rvm install 2.7 --with-openssl-dir=$HOME/.rvm/usr
bash --login && rvm use 2.7
ruby --version
```

## Jekyll

Install `jekill` as of the [installation](http://jekyllrb.com/docs/installation) instructions and as decribed by [so-simple-theme](https://github.com/mmistakes/so-simple-theme#ruby-gem-method):

```
gem install jekyll
gem install bundler
bundle install
bundle update --bundler
```

Serve the site:

```
jekyll serve -l -H localhost
```

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/metacodez/metacodes-blog/issues)
* Writing related blog articles
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`METACODES.PRO`](http://www.metacodes.pro) meta group of artifacts is published under some open source licenses; covered by the  [`metacodes-licensing`](https://bitbucket.org/metacodes/metacodes-licensing) ([`org.metacodes`](https://bitbucket.org/metacodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.

## Credits ##

This blog has been set up using [Michael Rose](https://github.com/mmistakes)'s great [So Simple Theme](http://mmistakes.github.io/so-simple-theme/theme-setup) theme; powered by the [jekyll](http://jekyllrb.com) templating engine for static web-sites ("*Jekyll • Simple, blog-aware, static sites*").
